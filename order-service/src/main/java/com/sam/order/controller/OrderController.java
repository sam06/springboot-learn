package com.sam.order.controller;

import com.sam.common.Result;
import com.sam.core.entity.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @classname: OrderController
 * @description: TODO
 * @Author: sam
 * @date: 2021/3/23 10:45
 */
@Slf4j
@RequestMapping("order")
@RestController
public class OrderController {

    @GetMapping("list")
    public Result<List<Order>> list()throws Exception{
        TimeUnit.SECONDS.sleep(2);
        return Result.ok(orderList());
    }

    private List<Order> orderList(){
        List<Order> orders= new ArrayList<>();
        for (int i=10000;i<=11000;i++){
            Order order = new Order();
            order.setOrderNo("SN"+i);
            order.setQty(5L);
            order.setTotalPrice(order.getQty() * i);
            order.setUserId("user1");
            orders.add(order);
        }
        return orders;
    }
}
