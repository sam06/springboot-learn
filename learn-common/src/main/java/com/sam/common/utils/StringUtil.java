package com.sam.common.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @classname: StringUtil
 * @description: TODO
 * @Author: sam
 * @date: 2021/4/15 18:04
 */
public class StringUtil {
    /**
     * 默认替换值
     */
    public static final String DEFAULT_NULL_REPLACE_VAL="";
    /**
     * 简单实现${}模板功能
     * 如${aa} cc ${bb} 其中 ${aa}, ${bb} 为占位符. 可用相关变量进行替换
     * @param templateStr 模板字符串
     * @param data     替换的变量值
     * @param defaultNullReplaceVals  默认null值替换字符, 如果不提供, 则为字符串""
     * @return 返回替换后的字符串, 如果模板字符串为null, 则返回null
     */
    @SuppressWarnings("unchecked")
    public static String render(String templateStr, Map<String, ?> data, String... defaultNullReplaceVals){
        if (templateStr == null){
            return null;
        }
        if(data == null) {
            data = Collections.EMPTY_MAP;
        }
        String nullReplaceVal = defaultNullReplaceVals.length > 0 ? defaultNullReplaceVals[0] : "";
        Pattern pattern = Pattern.compile("\\$\\{([^}]+)}");

        StringBuffer newValue = new StringBuffer(templateStr.length());

        Matcher matcher = pattern.matcher(templateStr);
        while (matcher.find()) {
            String key = matcher.group(1);
            String r = data.get(key) != null ? data.get(key).toString() : nullReplaceVal;
            matcher.appendReplacement(newValue, r.replaceAll("\\\\", "\\\\\\\\")); //这个是为了替换windows下的文件目录在java里用\\表示
        }
        matcher.appendTail(newValue);
        return newValue.toString();
    }
    /**
     * 简单实现${}模板功能
     * 如${aa} cc ${bb} 其中 ${aa}, ${bb} 为占位符. 可用相关变量进行替换
     * @param templateStr 模板字符串
     * @param data     替换的变量值
     * @return 返回替换后的字符串, 如果模板字符串为null, 则返回null
     */
    public static String render(String templateStr, Map<String, ?> data){
        return render(templateStr,data,DEFAULT_NULL_REPLACE_VAL);
    }

    public static void main(String[] args) {
        String tmpLine = "简历:\n 姓名: ${姓} ${名} \n 性别: ${性别}\n 年龄: ${年龄} \n";
        Map<String, String> data = new HashMap<String, String>();
        data.put("姓", "wen");
        data.put("名", "66");
        data.put("性别", "man");
        data.put("年龄", "222");

        System.out.println(render(tmpLine, data, "--"));
    }
}
