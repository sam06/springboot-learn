package com.sam.common.mq;

/**
 * @classname: Constant
 * @description: rabbit mq常量
 * @Author: sam
 * @date: 2021/9/20 11:13
 */
public final class RabbitConstant {

    /*定义交换机名称*/
    public static  final String EXCHANGE_NAME="boot_topic_exchange";
    /*定义队列名称*/
    public static final String QUEUE_NAME = "boot_queue";


    public static final String SIMPLE_QUEUE_NAME = "boot_queue";

    //*********************  日志   *********************

    public static  final String LOG_EXCHANGE_NAME="log_direct_exchange";

    public static final String LOG_QUEUE_NAME = "log_queue";

    public static final String LOG_KEY = "log";


    //*********************  订单队列   *********************

    public static final String ORDER_ROUTE_KEY = "order_route_key";
    public static final String ORDER_EXCHANGE = "order_exchange";
    public static final String ORDER_QUEUE = "order_queue_test";

    //死信队列

    public static final String DEAD_QUEUE = "dead_queue";
    public static final String DEAD_EXCHANGE = "dead_exchange";
    public static final String DEAD_ROUTE_KEY = "dead_route_key";

}
