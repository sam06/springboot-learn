package com.sam.test;

import com.sam.common.utils.StringUtil;
import org.apache.commons.text.StringEscapeUtils;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @classname: SampleTest
 * @description: TODO
 * @Author: sam
 * @date: 2021/4/15 17:53
 */
public class SampleTest {

    @Test
    public void test(){
        String html="<html>\n" +
                "<body>\n" +
                "<p>Hello World:${name}\n" +
                "</body>\n" +
                "</html>";

        System.out.println(html);
        System.out.println("======================转义");
        String escapeHtml = StringEscapeUtils.escapeHtml4(html);
        System.out.println(StringEscapeUtils.escapeHtml4(escapeHtml));

        System.out.println("======================反转义");
        System.out.println(StringEscapeUtils.unescapeHtml4(escapeHtml));

        Map<String,String> data = new HashMap<>();
        data.put("name","sam");
        System.out.println(StringUtil.render(html, data));
    }

    @Test
    public void testGet(){
        /*TemplateInfo templateInfo = templateInfoService.getByTempletCode("INVOICE_PARENT_SPECIFY");
        String content = templateInfo.getContent();
        String templetContent = StringEscapeUtils.unescapeHtml(content);
        System.out.println(templetContent);*/

        String html="<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "</head>\n" +
                "<body>\n" +
                "<p>Dear Parent Name：${clientName}<br>Please find your invoice for last month. No action required as you have a direct debit.<br>If you have any questions, don't hesitate to get in touch.<br>Thank you very much and have a lovely day.<br>\n" +
                "<br>\n" +
                "</p>\n" +
                "<p>Cick link to download your invoice:<a href=\"${linkUrl}\">${linkUrl}</a></p>\n" +
                "</body>\n" +
                "</html>";

        String templetContent = StringEscapeUtils.escapeHtml4(html);
        System.out.println("INVOICE_PARENT_SPECIFY");
        System.out.println(templetContent);

        System.out.println("=================================");

        Map<String,String> data = new HashMap<>(2);
        data.put("clientName",String.join(" ","aaa","bbb"));
        data.put("linkUrl",String.join("/","http://www.baidu.com","000000000000000"));

        String emailContent = StringUtil.render(html,data);
        System.out.println(emailContent);
        System.out.println("==========================================");
        System.out.println("INVOICE_PARENT_OTHER");
        String otherHtml="<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "</head>\n" +
                "<body>\n" +
                "<p>Dear Parent Name:${clientName}<br>Please find attached our invoice for tuition last month which is due in 7 days. <br><br>Please make payment to Barclays Bank<br>Sort Code 20 10 53<br>Acc 50823848<br>IBAN: GB05 BUKB 20105350823848<br>Swift BIC Code: BUKBGB22<br>If you have any questions, don't hesitate to get in touch.<br>Thank you very much and have a lovely day.<br>\n" +
                "</p>\n" +
                "<p>Cick link to download your invoice:<a href=\"${linkUrl}\">${linkUrl}</a></p>\n" +
                "</body>\n" +
                "</html>";


        String otherTempletContent = StringEscapeUtils.escapeHtml4(otherHtml);
        System.out.println(otherTempletContent);
    }
}
