package com.sam.mq.consumer;

import com.sam.common.mq.RabbitConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @classname: LogConsumer
 * @description: 日志消费端
 * @Author: sam
 * @date: 2021/9/20 11:04
 */
@Slf4j
@Component
public class LogConsumer {

    @RabbitListener(queues = RabbitConstant.LOG_QUEUE_NAME)
    public void log(String msg){
        log.info("接收到日志消息,{}",msg);
    }
}
