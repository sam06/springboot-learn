package com.sam.mq.consumer;

import com.rabbitmq.client.Channel;
import com.sam.common.mq.RabbitConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @classname: OrderConsumer
 * @description: TODO
 * @Author: sam
 * @date: 2021/9/20 18:10
 */
@Slf4j
@Component
public class OrderConsumer {

    /**
     *  监听你订单队列
     */
    @RabbitListener(queues = RabbitConstant.ORDER_QUEUE)
    public void orderQueueListener2(Message message, Channel channel) throws IOException, InterruptedException {
        String receivedRoutingKey = message.getMessageProperties().getReceivedRoutingKey();
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        String msg = new String(message.getBody());
        log.info("接收到订单消息:{}",msg);
        log.info("路由key:{},deliveryTag:{}",receivedRoutingKey,deliveryTag);
        Thread.sleep(5000);
        // 发送ack给消息队列,收到消息了
        channel.basicAck(deliveryTag,true);
    }

    // 监听订单队列
//    @RabbitListener(queues = RabbitConstant.ORDER_QUEUE)
    public void orderQueueListener(Message message,Channel channel) throws IOException {
        String receivedRoutingKey = message.getMessageProperties().getReceivedRoutingKey();
        String msg = new String(message.getBody());
        System.out.println("路由key= [ "+receivedRoutingKey+" ]接收到的消息= [ "+msg +" ]");
        //Thread.sleep(5000);
        //发送ack给消息队列,收到消息了
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),true);
    }

    // 监听死信队列
    @RabbitListener(queues = RabbitConstant.DEAD_QUEUE)
    public void deadQueueListener(Message message, Channel channel) throws InterruptedException, IOException {
        String receivedRoutingKey = message.getMessageProperties().getReceivedRoutingKey();
        String msg = new String(message.getBody());
        log.info("路由key= [ "+receivedRoutingKey+" ]接收到的消息= [ "+msg +" ]");
        Thread.sleep(5000);
        // 发送ack给消息队列，收到消息了
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),true);

    }
}
