package com.sam.mq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.sam.common.mq.RabbitConstant.LOG_QUEUE_NAME;

/**
 * @classname: RabbitConfig
 * @description: TODO
 * @Author: sam
 * @date: 2021/9/20 11:33
 */
@Configuration
public class RabbitConfig {
    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean("logQueue")
    public Queue logQueue(){
        return QueueBuilder.durable(LOG_QUEUE_NAME).build();
    }
}
