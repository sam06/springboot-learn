package com.sam.wechat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @classname: WxApp
 * @description: TODO
 * @Author: sam
 * @date: 2022/5/22 19:07
 */
@SpringBootApplication
public class WxApp {
    public static void main(String[] args) {
        SpringApplication.run(WxApp.class,args);
    }
}
