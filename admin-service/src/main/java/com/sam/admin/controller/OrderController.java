package com.sam.admin.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sam.admin.config.HttpClientUtil;
import com.sam.common.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @classname: OrderController
 * @description: TODO
 * @Author: sam
 * @date: 2021/3/23 11:21
 */
@Slf4j
@RequestMapping("order")
@RestController
public class OrderController {

    @Autowired
    private HttpClientUtil httpClientUtil;


    @GetMapping("batchList")
    public Result batchList()throws Exception{
        long start = System.currentTimeMillis();
        String url="http://localhost:8082/order/list";
        for (int i=0;i<100;i++){
            HttpResponse httpResponse = httpClientUtil.doGet(url, null, null);
            if (HttpStatus.SC_OK!=httpResponse.getStatusLine().getStatusCode()){
                return Result.error(500,"order服务调用失败！");
            }
            String emp_content = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
            EntityUtils.consume(httpResponse.getEntity());
            JSONObject empjsonObject = JSON.parseObject(emp_content);
            //log.info("{}",empjsonObject);
        }
        long end = System.currentTimeMillis();
        return Result.ok(end-start);
    }

    @GetMapping("list")
    public Result list()throws Exception{
        long start = System.currentTimeMillis();
        String url="http://localhost:8082/order/list";
        HttpResponse httpResponse = httpClientUtil.doGet(url, null, null);
        if (HttpStatus.SC_OK!=httpResponse.getStatusLine().getStatusCode()){
            return Result.error(500,"order服务调用失败！");
        }
        String emp_content = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
        EntityUtils.consume(httpResponse.getEntity());
        JSONObject empjsonObject = JSON.parseObject(emp_content);
        //log.info("{}",empjsonObject);
        long end = System.currentTimeMillis();
        return Result.ok(end-start);
    }
}
