package com.sam.core.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @classname: Order
 * @description: TODO
 * @Author: sam
 * @date: 2021/3/23 11:06
 */
@Data
public class Order implements Serializable {
    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 数量
     */
    private Long qty;

    /**
     * 总价（分）
     */
    private Long totalPrice;

    /**
     * 所属用户
     */
    private String userId;
}
