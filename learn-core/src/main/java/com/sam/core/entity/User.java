package com.sam.core.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @classname: User
 * @description: TODO
 * @Author: sam
 * @date: 2021/3/23 10:59
 */
@Data
public class User implements Serializable {
    private String userId;

    private String username;

    private String password;

    private String email;

    private String address;

    private String idCard;
}
