package com.springboot.learn.chapter02.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @classname: PersonController
 * @description: TODO
 * @Author: sam
 * @date: 2022/6/18 22:24
 */
@Slf4j
@RestController
public class PersonController {

    @PostMapping("valid/test1")
    public String test1(@RequestBody @Validated Person person){
        log.info("valid persion:{}",person);
        return "test1 valid success";
    }
}
