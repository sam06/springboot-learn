package com.springboot.learn.chapter02.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @classname: HelloDockerController
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/25 15:20
 */
@RestController
public class HelloDockerController {
    @GetMapping("/hello")
    public String hello() {
        return "hello docker!";
    }
}
