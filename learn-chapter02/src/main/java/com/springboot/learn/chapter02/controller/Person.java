package com.springboot.learn.chapter02.controller;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 * @classname: Person
 * @description: TODO
 * @Author: sam
 * @date: 2022/6/18 22:12
 */
@Data
public class Person {
    private String id;

    @Length(min = 6,max = 12,message = "appId长度必须位于6~12之间")
    private String appId;

    @NotBlank(message = "名字为必填项")
    private String name;

    @Email(message = "请输入正确的邮箱格式")
    private String email;

    private String sex;

    @NotEmpty(message = "级别不能为空")
    private String level;
}
