package com.sam.learn.shiro.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @classname: WebConfig
 * @description:
 * @Author: sam
 * @date: 2022/6/19 16:29
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    /**
     * 映射请求跳转
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }
}
