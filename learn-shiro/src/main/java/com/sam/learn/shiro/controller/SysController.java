package com.sam.learn.shiro.controller;

import com.sam.learn.shiro.vo.LoginVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @classname: LoginController
 * @description: TODO
 * @Author: sam
 * @date: 2022/6/19 16:56
 */
@Slf4j
@Controller
@RequestMapping("sys")
public class SysController {

    @PostMapping("login")
    public String login(@RequestBody LoginVo loginVo){
        Subject subject = SecurityUtils.getSubject();
        subject.login(new UsernamePasswordToken(loginVo.getUsername(),loginVo.getPassword()));
        return "index";
    }

    @GetMapping("say")
    public String say(String name){
        log.info("hello:"+name);
        return "index";
    }
}
