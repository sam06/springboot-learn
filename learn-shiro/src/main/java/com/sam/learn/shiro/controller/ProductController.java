package com.sam.learn.shiro.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @classname: ProductController
 * @description: TODO
 * @Author: sam
 * @date: 2022/6/19 22:51
 */
@RestController
@RequestMapping("product")
public class ProductController {

    @GetMapping("list")
    public String list(){
        return "ok";
    }
}
