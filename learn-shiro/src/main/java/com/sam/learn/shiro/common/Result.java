package com.sam.learn.shiro.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @classname: Result
 * @description: TODO
 * @Author: sam
 * @date: 2022/6/20 22:38
 */
@Data
public class Result<T> implements Serializable {
    /** 结果状态 ,具体状态码参见ResultData.java*/
    private int status;
    private String message;
    private T data;
    private long timestamp ;

    public Result (){
        this.timestamp = System.currentTimeMillis();
    }


    public static <T> Result<T> success(T data) {
        Result<T> result = new Result<>();
        result.setStatus(ResultCode.RC100.getCode());
        result.setMessage(ResultCode.RC100.getMessage());
        result.setData(data);
        return result;
    }

    public static <T> Result<T> fail(int code, String message) {
        Result<T> result = new Result<>();
        result.setStatus(code);
        result.setMessage(message);
        return result;
    }
}
