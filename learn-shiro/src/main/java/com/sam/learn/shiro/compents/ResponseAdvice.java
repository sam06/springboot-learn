package com.sam.learn.shiro.compents;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sam.learn.shiro.common.Result;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * @classname: ResponseAdvice
 * @description: TODO
 * @Author: sam
 * @date: 2022/6/20 22:46
 */
@Slf4j
@RestControllerAdvice
public class ResponseAdvice implements ResponseBodyAdvice<Object> {
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 是否支持advice功能,true 支持，false 不支持
     * @param methodParameter
     * @param aClass
     * @return
     */
    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    @SneakyThrows
    @Override
    public Object beforeBodyWrite(Object o,
                                  MethodParameter methodParameter,
                                  MediaType mediaType,
                                  Class<? extends HttpMessageConverter<?>> aClass,
                                  ServerHttpRequest serverHttpRequest,
                                  ServerHttpResponse serverHttpResponse) {
        log.info("object:{}",o);
        log.info("MethodParameter:{}",methodParameter);
        log.info("MediaType:{}",mediaType);
        log.info("Class:{}",aClass);
        if (o instanceof String){
            return objectMapper.writeValueAsString(Result.success(o));
        }
        return Result.success(o);
    }
}
