package com.sam.learn.shiro.controller;

import com.sam.learn.shiro.common.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @classname: OrderController
 * @description: TODO
 * @Author: sam
 * @date: 2022/6/19 22:52
 */
@RestController
@RequestMapping("order")
public class OrderController {
    @GetMapping("list")
    public Result<String> list(){
        return Result.success("order list");
    }
}
