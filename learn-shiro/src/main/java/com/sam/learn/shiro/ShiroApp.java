package com.sam.learn.shiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @classname: App
 * @description: TODO
 * @Author: sam
 * @date: 2022/6/19 11:30
 */
@SpringBootApplication
public class ShiroApp {
    public static void main(String[] args) {
        SpringApplication.run(ShiroApp.class,args);
    }
}
