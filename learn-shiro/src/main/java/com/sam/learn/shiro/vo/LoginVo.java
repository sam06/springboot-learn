package com.sam.learn.shiro.vo;

import lombok.Data;

/**
 * @classname: LoginVo
 * @description: TODO
 * @Author: sam
 * @date: 2022/6/19 16:57
 */
@Data
public class LoginVo {
    private String username;
    private String password;
}
