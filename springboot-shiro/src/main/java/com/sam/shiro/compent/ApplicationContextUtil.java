package com.sam.shiro.compent;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @classname: ApplicationContextUtil
 * @description: 获取对象工具类
 * @Author: sam
 * @date: 2021/11/14 19:12
 */
@Component
public class ApplicationContextUtil implements ApplicationContextAware {
    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    /**
     * 根据bean名字获取工厂中指定bean 对象
     */
    public static Object getBean(String beanName) {
        return context.getBean(beanName);
    }
}
