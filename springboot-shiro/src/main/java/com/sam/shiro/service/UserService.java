package com.sam.shiro.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.shiro.bean.User;

/**
 * @classname: UserService
 * @description: TODO
 * @Author: sam
 * @date: 2021/11/14 17:51
 */
public interface UserService extends IService<User> {
    User queryByName(String username);
}
