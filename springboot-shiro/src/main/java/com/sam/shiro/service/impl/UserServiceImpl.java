package com.sam.shiro.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.shiro.bean.User;
import com.sam.shiro.mapper.UserMapper;
import com.sam.shiro.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @classname: UserServiceImpl
 * @description: TODO
 * @Author: sam
 * @date: 2021/11/14 17:52
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Override
    public User queryByName(String username) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUsername,username);
        return this.baseMapper.selectOne(queryWrapper);
    }
}
