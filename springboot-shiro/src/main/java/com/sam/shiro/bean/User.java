package com.sam.shiro.bean;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @classname: User
 * @description: TODO
 * @Author: sam
 * @date: 2021/11/7 20:06
 */
@Data
@TableName(value = "sys_user")
public class User implements Serializable {

    private Long id;

    private String username;

    private String nickName;

    private String password;

    private String salt;

}
