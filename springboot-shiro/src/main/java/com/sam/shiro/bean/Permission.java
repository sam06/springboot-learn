package com.sam.shiro.bean;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @classname: Permission
 * @description: TODO
 * @Author: sam
 * @date: 2021/11/7 20:10
 */
@Data
@TableName(value = "sys_Permission")
public class Permission implements Serializable {

    private Long id;

    private String name;

    private String code;

    private String url;

    private String description;
}
