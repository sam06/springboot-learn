package com.sam.shiro.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @classname: Role
 * @description: TODO
 * @Author: sam
 * @date: 2021/11/7 20:09
 */
@Data
@TableName(value = "sys_role")
public class Role implements Serializable {
    @TableId
    private Long id;

    private String roleName;

    private String roleCode;

    private String description;
}
