package com.sam.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.shiro.bean.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @classname: UserMapper
 * @description: TODO
 * @Author: sam
 * @date: 2021/11/7 20:40
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
