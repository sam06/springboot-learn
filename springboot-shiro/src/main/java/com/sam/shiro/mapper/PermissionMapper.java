package com.sam.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.shiro.bean.Permission;
import org.apache.ibatis.annotations.Mapper;

/**
 * @classname: PermissionMapper
 * @description: TODO
 * @Author: sam
 * @date: 2021/11/7 20:40
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {
}
