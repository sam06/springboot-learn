package com.sam.shiro;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @classname: ShiroApp
 * @description: TODO
 * @Author: sam
 * @date: 2021/11/7 20:41
 */
@MapperScan("com.sam.shiro.mapper")
@SpringBootApplication
public class ShiroApp {
    public static void main(String[] args) {
        SpringApplication.run(ShiroApp.class,args);
    }
}
