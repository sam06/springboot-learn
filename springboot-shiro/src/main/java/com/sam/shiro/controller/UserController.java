package com.sam.shiro.controller;

import com.sam.shiro.bean.User;
import com.sam.shiro.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @classname: UserController
 * @description: TODO
 * @Author: sam
 * @date: 2021/11/14 19:44
 */
@RequestMapping("user")
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("list")
    public List<User> list(){
        return userService.list();
    }
}
