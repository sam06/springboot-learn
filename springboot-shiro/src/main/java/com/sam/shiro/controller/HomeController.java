package com.sam.shiro.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @classname: HomeController
 * @description: TODO
 * @Author: sam
 * @date: 2021/11/7 21:46
 */
@Slf4j
@Controller
public class HomeController {
    @RequestMapping({"/","/index"})
    public String index(){
        return"/index";
    }

    // 跳转到登录表单页面
    @RequestMapping(value="login")
    public String login() {
        return "login";
    }


    @PostMapping("login")
    public @ResponseBody Map<String,Object> login(String username, String password, Model model){
        log.info("登录操作,username:{},password:{}",username,password);
        Map<String, Object> resultMap = new LinkedHashMap<String, Object>();
        try {
            UsernamePasswordToken token = new UsernamePasswordToken(username, password);
            SecurityUtils.getSubject().login(token);

            resultMap.put("code",200);
            resultMap.put("msg","登录成功");
        }catch (Exception ex){
            resultMap.put("code",500);
            resultMap.put("msg",ex.getMessage());
        }

        return resultMap;
    }
}
