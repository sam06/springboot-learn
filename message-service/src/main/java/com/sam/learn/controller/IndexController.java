package com.sam.learn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.internet.MimeMessage;
import java.io.File;

/**
 * @classname: IndexController
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/20 22:40
 */
@RestController
public class IndexController {

    @Autowired
    private JavaMailSender javaMailSender;

    @GetMapping("/{name}")
    public String hello(@PathVariable String name){

        return "hello:"+name;
    }

    @RequestMapping("sendSimpleEmail")
    public String sendSimpleEmail(){

        SimpleMailMessage message = new SimpleMailMessage();
        // 发件人
        message.setFrom("13918065127@163.com");
        // 收件人
        message.setTo("13918065127@163.com");
        // 主题
        message.setSubject("测试简单邮件");
        // 内容
        message.setText("springboot 简单邮件");
        javaMailSender.send(message);
        return "发送成功";
    }

    /**
     * 发送html邮件，内容(可以包含html等标签)
     * @return
     */
    @RequestMapping("sendHtmlEmail")
    public String sendHtmlEmail() throws Exception{
        // 使用MimeMessage，MIME协议
        MimeMessage message = javaMailSender.createMimeMessage();

        MimeMessageHelper helper;
        // MimeMessageHelper帮助我们设置更丰富的内容
        helper = new MimeMessageHelper(message,true);
        helper.setFrom("13918065127@163.com");
        helper.setTo("13918065127@163.com");
        helper.setSubject("测试html邮件");

        String content = "<html><body><h3><font color=\"red\">" + "大家好，这是springboot发送的HTML邮件" + "</font></h3></body></html>";
        helper.setText(content,true);
        javaMailSender.send(message);
        return "发送html邮件成功";
    }

    /**
     *  带附件的邮件
     * @return
     * @throws Exception
     */
    @RequestMapping("sendAttachmentMail")
    public String sendAttachmentMail()throws Exception{
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper;

        helper = new MimeMessageHelper(message,true);
        helper.setFrom("13918065127@163.com");
        helper.setTo("13918065127@163.com");
        helper.setSubject("带附件的邮件");
        // html正文
        String content = "<html><body><h3><font color=\"red\">" + "大家好，这是springboot发送的HTML邮件，有附件哦" + "</font></h3></body></html>";
        helper.setText(content,true);

        // 附件
        String filePath = "C:\\Users\\zhipe\\Pictures\\pingtouge.jpeg";
        FileSystemResource file = new FileSystemResource(new File(filePath));
        String filename = file.getFilename();
        //添加附件，可多次调用该方法添加多个附件
        helper.addAttachment(filename,file);

        javaMailSender.send(message);
        return "发送带附件的邮件";
    }
}
