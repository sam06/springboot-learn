package com.sam.learn.controller;

import com.sam.learn.entity.StudentEntity;
import com.sam.learn.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @classname: StudentController
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/30 11:34
 */
@RequestMapping("student")
@RestController
public class StudentController {
    @Autowired
    private StudentService studentService;

    @PostMapping("/put")
    public StudentEntity saveStudent(@RequestBody StudentEntity student){
        return studentService.saveStudent(student);
    }

    @DeleteMapping("/evit/{id}")
    public void deleteStudentById(@PathVariable("id") Integer id){
        studentService.deleteStudentById(id);
    }

    @GetMapping("/able/{id}")
    public StudentEntity findStudentById(@PathVariable("id") Integer id){
        return studentService.findStudentById(id);
    }
}
