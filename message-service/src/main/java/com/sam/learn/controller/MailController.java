package com.sam.learn.controller;

import com.sam.learn.dto.MailDTO;
import com.sam.learn.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @classname: MailController
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/23 22:25
 */
@RequestMapping("mail")
@RestController
public class MailController {

    @Autowired
    private MailService mailService;

    @RequestMapping("send")
    public String send(@RequestBody MailDTO mailDTO){
        mailService.send(mailDTO);
        return "发送成功";
    }

    @RequestMapping("clear")
    public void clear(){
        mailService.clear();
    }
}
