package com.sam.learn;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @classname: MessageApp
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/20 22:38
 */
@EnableCaching
@MapperScan("com.sam.learn.mapper")
@SpringBootApplication
public class MessageApp {
    public static void main(String[] args) {
        SpringApplication.run(MessageApp.class,args);
    }
}
