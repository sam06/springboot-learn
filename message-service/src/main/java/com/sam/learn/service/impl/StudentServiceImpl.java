package com.sam.learn.service.impl;

import com.sam.learn.entity.StudentEntity;
import com.sam.learn.mapper.StudentMapper;
import com.sam.learn.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @classname: StudentServiceImpl
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/30 11:24
 */
@Slf4j
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentMapper studentMapper;

    /**
     * 新增的或更新的
     * @param studentEntity
     * @return
     */
    @Override
    @CachePut(value = "student",key = "#studentEntity.id")
    public StudentEntity saveStudent(StudentEntity studentEntity) {
        int insert = studentMapper.insert(studentEntity);
        log.info("为id、key 为{}的数据做了缓存", studentEntity.getId());
        return studentEntity;
    }

    @Override
    @CacheEvict(value = "student")
    public void deleteStudentById(Integer id) {
        studentMapper.deleteById(id);
        log.info("删除了id、key 为{}的数据缓存", id);
    }

    @Override
    @Cacheable(value = "student",key = "#id")
    public StudentEntity findStudentById(Integer id) {
        StudentEntity studentEntity = studentMapper.selectById(id);
        log.info("为id、key 为{}的数据做了缓存",id);
        return studentEntity;
    }
}
