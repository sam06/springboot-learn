package com.sam.learn.service;

import com.sam.learn.entity.StudentEntity;

/**
 * @classname: StudentService
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/30 11:24
 */
public interface StudentService {
    public StudentEntity saveStudent(StudentEntity studentEntity);

    public void deleteStudentById(Integer id);

    public StudentEntity findStudentById(Integer id);
}
