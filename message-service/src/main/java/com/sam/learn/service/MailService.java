package com.sam.learn.service;

import com.sam.learn.dto.MailDTO;

/**
 * @classname: MailService
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/23 22:21
 */
public interface MailService {

    void send(MailDTO mailDTO);

    void clear();
}