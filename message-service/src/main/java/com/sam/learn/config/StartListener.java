package com.sam.learn.config;

import com.sun.istack.internal.NotNull;
import lombok.SneakyThrows;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * @classname: StartListener
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/24 23:06
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class StartListener implements ApplicationListener<ApplicationStartedEvent> {
    MailSenderConfig mailSenderConfig;

    public StartListener(MailSenderConfig mailSenderConfig) {
        this.mailSenderConfig = mailSenderConfig;
    }

    @SneakyThrows
    @Override
    public void onApplicationEvent(@NotNull ApplicationStartedEvent event) {
        this.mailSenderConfig.buildMailSender();
    }
}
