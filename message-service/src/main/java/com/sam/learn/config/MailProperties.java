package com.sam.learn.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @classname: MailProperties
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/23 22:05
 */
@Data
@Component
@ConfigurationProperties(prefix = "spring.mail")
public class MailProperties {

    /**  * 用户名 */
    private String username;
    /** * 授权码 */
    private String password;
    /** * host */
    private String host;
    /** * 端口 */
    private Integer port;
    /*** 协议 */
    private String protocol;
    /** * 默认编码*/
    private String defaultEncoding;
}
