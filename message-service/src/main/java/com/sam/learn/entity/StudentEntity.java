package com.sam.learn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @classname: Student
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/30 11:22
 */
@Data
@TableName("tb_student")
public class StudentEntity implements Serializable {

    @TableId(value = "id",type=IdType.AUTO)
    private Long id;

    private String name;

    private Integer age;
}
