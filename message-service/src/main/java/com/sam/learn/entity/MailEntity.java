package com.sam.learn.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @classname: MailEntity
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/23 22:08
 */
@Data
@TableName("mail_server")
public class MailEntity implements Serializable {
    private Long id;

    private String mailCode;

    private String host;

    private String username;

    private String password;

    private Integer port=465;

    /** * 协议 */
    private String protocol="smtps";

    private String defaultEncoding="utf-8";

    /**
     * 使用状态，1:正在使用，2:禁用，3:停用
     * TODO 后期应该更改为 枚举类来进行实现
     */
    private Integer state=1;

    /** * 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    /*** 修改时间 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime  updateTime;
}
