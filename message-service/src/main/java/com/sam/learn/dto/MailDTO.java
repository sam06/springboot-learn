package com.sam.learn.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @classname: MailDTO
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/23 22:23
 */
@Data
public class MailDTO implements Serializable {

    /*** 接受邮箱账户*/
    private String mail;
    /*** 邮箱标题*/
    private String title;
    /** * 要发送的内容*/
    private String content;
}
