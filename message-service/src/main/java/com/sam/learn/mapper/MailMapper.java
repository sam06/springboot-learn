package com.sam.learn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.learn.entity.MailEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @classname: MailMapper
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/23 22:20
 */
@Mapper
public interface MailMapper extends BaseMapper<MailEntity> {

}
