package com.sam.learn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.learn.entity.StudentEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @classname: StudentMapper
 * @description: TODO
 * @Author: sam
 * @date: 2022/10/30 11:23
 */
@Mapper
public interface StudentMapper extends BaseMapper<StudentEntity> {
}
