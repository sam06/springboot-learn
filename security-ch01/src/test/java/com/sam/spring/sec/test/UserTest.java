package com.sam.spring.sec.test;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.sam.spring.security.Security01App;
import com.sam.spring.security.entity.User;
import com.sam.spring.security.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @classname: UserTest
 * @description: TODO
 * @Author: sam
 * @date: 2021/7/10 22:03
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Security01App.class})
public class UserTest {

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    public void testPasswordEncoder(){
        System.out.println(passwordEncoder.encode("123"));
    }

    @Test
    public void testGet(){
        LambdaQueryWrapper<User> query = new LambdaQueryWrapper<User>();
        query.eq(User::getUsername, "admin");
        query.or().eq(User::getUsername, "dev");
        User user = userService.getOne(query.last("limit 1"));
        log.info("user:{}",user);
    }
}
