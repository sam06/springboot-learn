package com.sam.spring.security.config;

import com.sam.spring.security.properties.SecurityProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @classname: SecurityPropertiesConfig
 * @description: 使SecurityProperties生效
 * @Author: sam
 * @date: 2021/6/29 18:00
 */
@Configuration
@EnableConfigurationProperties(SecurityProperties.class)
public class SecurityPropertiesConfig {
}
