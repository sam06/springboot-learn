package com.sam.spring.security.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.spring.security.entity.UserRole;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sam
 * @since 2021-07-07
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
