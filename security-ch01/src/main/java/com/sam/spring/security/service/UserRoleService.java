package com.sam.spring.security.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.spring.security.entity.UserRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sam
 * @since 2021-07-07
 */
public interface UserRoleService extends IService<UserRole> {

}
