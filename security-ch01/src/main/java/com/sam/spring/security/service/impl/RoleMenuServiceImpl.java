package com.sam.spring.security.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.spring.security.entity.RoleMenu;
import com.sam.spring.security.mapper.RoleMenuMapper;
import com.sam.spring.security.service.RoleMenuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sam
 * @since 2021-07-07
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

}
