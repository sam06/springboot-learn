package com.sam.spring.security.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.spring.security.entity.Role;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sam
 * @since 2021-07-07
 */
public interface RoleService extends IService<Role> {
    List<String> getListByUserName(String userName);
}
