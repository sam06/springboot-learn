package com.sam.spring.security.support;

import java.io.Serializable;

/**
 * @classname: SimpleResponse
 * @description: 定制的返回对象
 * @Author: sam
 * @date: 2021/6/29 18:10
 */
public class SimpleResponse implements Serializable {
    public SimpleResponse (Object content) {
        this.content = content;
    }

    private Object content;

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

}
