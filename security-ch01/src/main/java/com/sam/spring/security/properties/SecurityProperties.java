package com.sam.spring.security.properties;

import com.sam.spring.security.enums.LoginTypeEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @classname: SecurityProperties
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/29 17:36
 */
@Data
@ConfigurationProperties(prefix = "sam.security")
public class SecurityProperties {
    /**
     * default json
     */
    private LoginTypeEnum loginType = LoginTypeEnum.JSON;
}
