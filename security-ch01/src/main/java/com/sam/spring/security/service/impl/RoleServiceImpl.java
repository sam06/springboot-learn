package com.sam.spring.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.spring.security.entity.Role;
import com.sam.spring.security.mapper.RoleMapper;
import com.sam.spring.security.service.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sam
 * @since 2021-07-07
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
    @Override
    public List<String> getListByUserName(String userName) {
        return baseMapper.getListByUserName(userName);
    }
}
