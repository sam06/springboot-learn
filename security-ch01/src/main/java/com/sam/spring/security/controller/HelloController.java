package com.sam.spring.security.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @classname: HelloController
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/27 16:41
 */

@RestController
public class HelloController {

    @GetMapping("hello")
    public String getWelcomeMsg() {
        return "Hello,Spring Security";
    }

    @GetMapping("/admin/hello")
    public String admin() {
        return "Hello,admin";
    }

    @GetMapping("/dba/hello")
    public String dba() {
        return "Hello,dba";
    }

    @GetMapping("/user/hello")
    public String user() {
        return "Hello,user";
    }
}
