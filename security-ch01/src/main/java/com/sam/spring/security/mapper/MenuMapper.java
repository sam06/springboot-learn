package com.sam.spring.security.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.spring.security.entity.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sam
 * @since 2021-07-07
 */
public interface MenuMapper extends BaseMapper<Menu> {
    /**
     * 根据角色集合，获取权限 列表
     * @param roleCodes
     * @return
     */
    List<String> queryMenuByRoleCods(@Param("roleCodes") List<String> roleCodes);
}
