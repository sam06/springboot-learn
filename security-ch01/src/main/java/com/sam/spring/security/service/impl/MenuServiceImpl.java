package com.sam.spring.security.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.spring.security.entity.Menu;
import com.sam.spring.security.mapper.MenuMapper;
import com.sam.spring.security.service.MenuService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sam
 * @since 2021-07-07
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {
    /**
     * 根据角色集合，查询权限列表
     *
     * @param roleCodes
     * @return
     */
    @Override
    public List<String> queryMenuByRoleCods(List<String> roleCodes) {
        return baseMapper.queryMenuByRoleCods(roleCodes);
    }
}
