package com.sam.spring.security.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.spring.security.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sam
 * @since 2021-07-07
 */
public interface UserService extends IService<User> {

    /**
     * 根据用户名获取用户信息
            
     * @Author: sam
     * @Date: 2021/7/11 16:29
     **/
    User getByUserName(String userName);
}
