package com.sam.spring.security.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.spring.security.entity.User;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sam
 * @since 2021-07-07
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}
