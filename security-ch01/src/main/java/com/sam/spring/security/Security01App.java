package com.sam.spring.security;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @classname: Security01App
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/27 16:40
 */
@MapperScan("com.sam.spring.security.mapper")
@SpringBootApplication
public class Security01App {
    public static void main(String[] args) {
        SpringApplication.run(Security01App.class,args);
    }
}
