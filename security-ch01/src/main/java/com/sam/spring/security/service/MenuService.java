package com.sam.spring.security.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.spring.security.entity.Menu;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sam
 * @since 2021-07-07
 */
public interface MenuService extends IService<Menu> {
    List<String> queryMenuByRoleCods(List<String> roleCodes);
}
