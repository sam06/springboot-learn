package com.sam.spring.security.enums;

/**
 * @classname: LoginType
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/29 17:33
 */
public enum LoginTypeEnum {
    // 跳转
    REDIRECT,
    // 返回json
    JSON
}
