package com.sam.spring.security.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.spring.security.entity.User;
import com.sam.spring.security.mapper.UserMapper;
import com.sam.spring.security.service.UserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sam
 * @since 2021-07-07
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    /**
     * 根据用户名获取用户信息
     *
     * @param userName
     * @Author: sam
     * @Date: 2021/7/11 10:37
     */
    @Override
    public User getByUserName(String userName) {
        LambdaQueryWrapper<User> query = new LambdaQueryWrapper<User>();
        query.eq(User::getUsername, userName);
        query.or().eq(User::getMobile,userName);
        query.last("limit 1");
        // 根据用户名查询用户
        User user = this.baseMapper.selectOne(query);
        return user;
    }
}
