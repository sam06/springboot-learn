package com.sam.mybatisplus.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sam.common.utils.DateTool;
import com.sam.mybatisplus.dto.LessonVo;
import lombok.Data;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.util.stream.Collectors.toList;

/**
 * @classname: Java8Test
 * @description: TODO
 * @Author: sam
 * @date: 2021/4/9 16:10
 */
public class Java8Test {
    @Test
    public void testJosn(){
        /*String str="https://pclive.xuedianyun.com/pcBase/pclive2/dev/index.html?portalIP=saas.xuedianyun.com&portalPort=80&classId=773854708&playRecord=1";
        String s = JSON.parseObject(str,String.class);
        System.out.println(s);*/

        String s1="abc";
        String s2 = JSON.parseObject(s1,String.class);
        System.out.println(s2);
    }
    @Test
    public void test(){
        List<Time> timeList = new ArrayList<>();
        Time t1 = new Time();
        t1.setStartTime("2021-01-01");
        t1.setEndTime("2021-01-15");

        timeList.add(t1);

        Time t2 = new Time();
        t2.setStartTime("2021-01-10");
        t2.setEndTime("2021-01-16");

        timeList.add(t2);

        Time t3 = new Time();
        t3.setStartTime("2021-01-12");
        t3.setEndTime("2021-01-20");

        timeList.add(t3);

        String start = timeList.stream().map(Time::getStartTime).min((e1, e2) -> e1.compareTo(e2)).get();
        String end = timeList.stream().map(Time::getEndTime).max((e1, e2) -> e1.compareTo(e2)).get();
        System.out.println("max start time:"+start);
        System.out.println("max end time:"+end);
        /*返回负数表示：o1 小于o2，
        返回0 表示：o1和o2相等，
        返回正数表示：o1大于o2*/
        String s1="2021-01-01";
        String s2="2020-12-31";
        System.out.println(s1.compareTo(s2));
    }

    @Data
    class Time{
        String startTime;
        String endTime;
    }

    @Test
    public void test2(){
        List<String> list=new ArrayList<>();
        List<String> collect = list.stream().collect(toList());

        // 家长上月overpayment记录总额+（上个月已支付总金额 - 上个月invoice的含税总额），取反欠款显示正数 提前预付显示负数;
        BigDecimal lastMonthAmt = new BigDecimal("100");
        BigDecimal totalGrossAmount = new BigDecimal("500");
        BigDecimal totalPaymentAmount = new BigDecimal("200");
        BigDecimal amount = lastMonthAmt.add(totalPaymentAmount.subtract(totalGrossAmount));
        System.out.println("amount="+amount);

        BigDecimal balance = amount;
        System.out.println("balance="+balance);

        System.out.println("===================");
        balance = balance.add(new BigDecimal("100"));
        System.out.println("balance="+balance);
        System.out.println("amount="+amount);
    }

    @Test
    public void test3(){
        Map<String,String> map = new HashMap<>();
        map.put("111111","AAAAAAAAAAAAAAAAAAAAAA");

        Map<String,String> map2 = new HashMap<>();
        map2.put("222222","BBBBBBBBBBBBBBBBBBBBBBBB");

        List<Map<String,String>> list = new ArrayList<>();
        list.add(map);
        list.add(map2);

        System.out.println(JSONObject.toJSON(list));
        Map<String,String> resultMap = new HashMap<>();
        list.stream().forEach(item->{
            resultMap.putAll(item);
        });

        System.out.println(resultMap);
    }

    @Test
    public void testTimeZone(){
        String[] availableIDs = TimeZone.getAvailableIDs();
        for (String id:availableIDs){
            System.out.println(id);
        }
    }

    @Test
    public void testConvertZone(){
        String now = "2021-05-12 10:00:00";
        System.out.println("北京时间:"+now);
        String currTimeZoneId = "Asia/Shanghai";
        // (UTC+00:00) 协调世界时 Etc/GMT
        String GMT = "Etc/GMT";

        // (UTC+00:00) 都柏林,爱丁堡,里斯本,伦敦 Europe/London
        String London = "Europe/London";

        // (UTC+01:00) 阿姆斯特丹,柏林,伯尔尼,罗马,斯德哥尔摩,维也纳 Europe/Berlin
        String Berlin = "Europe/Berlin";

        // (UTC-05:00) 东部时间（美国和加拿大） America/New_York
        String new_york = "America/New_York";

        // (UTC-06:00) 中部时间（美国和加拿大） America/Chicago
        String Chicago = "America/Chicago";


        String GMTTime = DateTool.timeZoneTransfer(now, DateTool.DATE_FORMAT, currTimeZoneId,GMT);
        String LondonTime = DateTool.timeZoneTransfer(now, DateTool.DATE_FORMAT, currTimeZoneId,London);
        String BerlinTime = DateTool.timeZoneTransfer(now, DateTool.DATE_FORMAT, currTimeZoneId,Berlin);
        String newYorkTime = DateTool.timeZoneTransfer(now, DateTool.DATE_FORMAT, currTimeZoneId,new_york);
        String ChicagoTime = DateTool.timeZoneTransfer(now, DateTool.DATE_FORMAT, currTimeZoneId,Chicago);
        System.out.println("(UTC+00:00) 协调世界时:"+GMTTime);
        System.out.println("(UTC+00:00) 都柏林,爱丁堡,里斯本,伦敦:"+LondonTime);
        System.out.println("(UTC+01:00) 阿姆斯特丹,柏林,伯尔尼,罗马,斯德哥尔摩,维也纳:"+BerlinTime);
        System.out.println("(UTC-05:00) 东部时间（美国和加拿大）:"+newYorkTime);
        System.out.println("(UTC-06:00) 中部时间（美国和加拿大）:"+ChicagoTime);
    }

    @Test
    public void testDate(){
        //1.具有转换功能的对象
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        //2.要转换的对象
        LocalDateTime time = LocalDateTime.now();

        //3.发动功能
        String localTime = df.format(time);
        System.out.println("LocalDateTime转成String类型的时间："+localTime);

        //3.LocalDate发动，将字符串转换成  df格式的LocalDateTime对象，的功能
        LocalDateTime LocalTime = LocalDateTime.parse("2021-04-23 00:00:01",df);
        System.out.println("String类型的时间转成LocalDateTime："+LocalTime);
    }

    @Test
    public void testConvert(){
        String startTime="2021-04-26 14:18:42";
        TimeZone utc = TimeZone.getTimeZone("UTC");
    }

    @Test
    public void testSub(){
        String str="12345678";
        String substring = str.substring(2, str.length());
        System.out.println(substring);

    }


    @Test
    public void testConvertDate()throws Exception{
        String dateString="2020-06-08 00:00:00.0";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = format.parse(dateString);
        System.out.println(parse);

        BigDecimal b1 = new BigDecimal("100");
        BigDecimal b2 = new BigDecimal("200");
        System.out.println(b1.subtract(b2));
        System.out.println(b1.negate());
    }

    @Test
    public void testList(){
        List<LessonVo> lessonVoList = new ArrayList<>();
        LessonVo lessonVo1= new LessonVo();
        lessonVo1.setLessonName("语文");
        lessonVo1.setType("文科");
        lessonVo1.setPrice(new BigDecimal("100"));
        lessonVoList.add(lessonVo1);

        LessonVo lessonVo2= new LessonVo();
        lessonVo2.setLessonName("数学");
        lessonVo2.setType("理科");
        lessonVo2.setPrice(new BigDecimal("150"));
        lessonVoList.add(lessonVo2);

        LessonVo lessonVo3 = new LessonVo();
        lessonVo3.setLessonName("物理");
        lessonVo3.setType("理科");
        lessonVo3.setPrice(new BigDecimal("-120"));
        lessonVoList.add(lessonVo3);

        LessonVo lessonVo4 = new LessonVo();
        lessonVo4.setLessonName("物理");
        lessonVo4.setType(null);
        lessonVo4.setPrice(new BigDecimal("100"));
        lessonVoList.add(lessonVo4);
        lessonVoList.stream().filter(item -> !Objects.isNull(item.getType())).forEach(System.out::println);
        BigDecimal reduce = lessonVoList.stream().filter(item -> !Objects.isNull(item.getType())).map(LessonVo::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(reduce);

        LessonVo lessonVo = JSON.parseObject("{}", LessonVo.class);
        System.out.println(lessonVo==null);
    }

    /**
     * 交集，差集
     */
    @Test
    public void test20(){
        List<LessonVo> lessonVoList = new ArrayList<>();
        LessonVo lessonVo1= new LessonVo();
        lessonVo1.setLessonName("语文");
        lessonVo1.setType("文科");
        lessonVo1.setPrice(new BigDecimal("100"));
        lessonVoList.add(lessonVo1);

        LessonVo lessonVo2= new LessonVo();
        lessonVo2.setLessonName("数学");
        lessonVo2.setType("理科");
        lessonVo2.setPrice(new BigDecimal("150"));
        lessonVoList.add(lessonVo2);

        List<LessonVo> lessonVoList2 = new ArrayList<>(lessonVoList);
        LessonVo lessonVo3 = new LessonVo();
        lessonVo3.setLessonName("物理");
        lessonVo3.setType("理科");
        lessonVo3.setPrice(new BigDecimal("-120"));
        lessonVoList2.add(lessonVo3);

        // 差集 (list1 - list2)
        List<LessonVo> collect = lessonVoList2.stream().filter(item -> !lessonVoList.contains(item)).collect(toList());
        System.out.println("---差集 reduce1 (list1 - list2)---");
        collect.parallelStream().forEach(System.out :: println);
    }


    @Test
    public void test21(){
        List<String> list1 = new ArrayList<String>();
        list1.add("1");
        list1.add("2");
        list1.add("3");
        list1.add("5");
        list1.add("6");

        List<String> list2 = new ArrayList<String>();
        list2.add("2");
        list2.add("3");
        list2.add("7");
        list2.add("8");

        // 交集
        List<String> intersection = list1.stream().filter(item -> list2.contains(item)).collect(toList());
        System.out.println("---交集 intersection---");
        intersection.parallelStream().forEach(System.out :: println);

        // 差集 (list1 - list2)
        List<String> reduce1 = list1.stream().filter(item -> !list2.contains(item)).collect(toList());
        System.out.println("---差集 reduce1 (list1 - list2)---");
        reduce1.parallelStream().forEach(System.out :: println);

        // 差集 (list2 - list1)
        List<String> reduce2 = list2.stream().filter(item -> !list1.contains(item)).collect(toList());
        System.out.println("---差集 reduce2 (list2 - list1)---");
        reduce2.parallelStream().forEach(System.out :: println);

        reduce1.addAll(reduce2);
        System.out.println("---对称差集");
        System.out.println(reduce1);
    }
}
