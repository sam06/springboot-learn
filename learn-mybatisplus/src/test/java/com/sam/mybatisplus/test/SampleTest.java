package com.sam.mybatisplus.test;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import com.sam.mybatisplus.dto.DictItemDTO;
import com.sam.mybatisplus.entity.DictEntity;
import com.sam.mybatisplus.entity.DictItemEntity;
import com.sam.mybatisplus.entity.User;
import com.sam.mybatisplus.mapper.TagMapper;
import com.sam.mybatisplus.mapper.UserMapper;
import com.sam.mybatisplus.service.DictItemService;
import com.sam.mybatisplus.service.DictService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @classname: MybatisTest
 * @description: TODO
 * @Author: sam
 * @date: 2021/3/26 16:06
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleTest {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DictService dictService;

    @Autowired
    private DictItemService dictItemService;

    @Resource
    private TagMapper tagMapper;

    @Test
    public void teatBatchSaveOrUpdate(){
    }
    /**
     * 测试断言
     */
    @Test
    public void testAssert(){
        /*User user = new User();
        Assert.notNull(user,"用户不能为空！");*/

        User user1 = userMapper.selectOne(new LambdaQueryWrapper<User>().eq(User::getUsername, "zhangsan"));
        System.out.println(user1);

        List<User> userList = new ArrayList<>();
        User user = new User();
        user.setId(1L);
        user.setAddress("更新");
        userList.add(user);
        userMapper.batchUpdate(userList);
    }

    /*@Autowired
    private UserService userService;

    @Test
    public void test(){
        List<UserEntity> userEntities = userMapper.selectList(null);
        userEntities.forEach(System.out::println);
    }

    @Autowired
    public void testAll(){
    }

    @Test
    public void testInsert(){
        UserEntity userEntity = new UserEntity();
        userEntity.setName("测试数据");
        userEntity.setAge(20);
        userEntity.setEmail("test@qq.com");

        int insert = userMapper.insert(userEntity);
    }*/

    /**
     * 测试删除(MybatisPlus之防止全表更新与删除插件BlockAttackInnerInterceptor)
     * 链接：https://blog.csdn.net/qq_43437874/article/details/114842098
     */
    @Test
    public void testDel(){
        int delete = userMapper.delete(null);
        System.out.println(delete);
    }

    @Test
    public void test3(){
        Map<String,String> map = new HashMap<>();
        map.put("111111","AAAAAAAAAAAAAAAAAAAAAA");

        Map<String,String> map2 = new HashMap<>();
        map2.put("222222","BBBBBBBBBBBBBBBBBBBBBBBB");

        List<Map<String,String>> list = new ArrayList<>();
        list.add(map);
        list.add(map2);

        Map<String,String> resultMap = new HashMap<>();
        list.stream().forEach(item->{
            resultMap.putAll(item);
        });

        System.out.println(resultMap);
    }

    @Test
    public void testSaveDict(){
        DictEntity dictEntity = new DictEntity();
        dictEntity.setDictCode("education");
        dictEntity.setDictName("学历");
        dictService.save(dictEntity);
    }

    @Test
    public void testGetDict(){
        List<DictItemDTO> educationList = dictItemService.getListByDictCode("education");
        System.out.println(educationList);
    }

    @Test
    public void testSaveDictItem(){
        saveGender("gender");
    }
    private void saveGender(String dictCode){
        List<DictItemEntity> dictItemEntityList = Lists.newArrayList();
        DictItemEntity dictItemEntity = new DictItemEntity();
        dictItemEntity.setDictCode(dictCode);
        dictItemEntity.setItemLabel("男");
        dictItemEntity.setItemValue("0");
        dictItemEntityList.add(dictItemEntity);

        DictItemEntity dictItemEntity2 = new DictItemEntity();
        dictItemEntity2.setDictCode(dictCode);
        dictItemEntity2.setItemLabel("女");
        dictItemEntity2.setItemValue("1");
        dictItemEntityList.add(dictItemEntity2);

        DictItemEntity dictItemEntity3 = new DictItemEntity();
        dictItemEntity3.setDictCode(dictCode);
        dictItemEntity3.setItemLabel("未知");
        dictItemEntity3.setItemValue("2");
        dictItemEntityList.add(dictItemEntity3);

        dictItemService.saveBatch(dictItemEntityList);
    }
    private void saveEducation(){
        List<DictItemEntity> dictItemEntityList = Lists.newArrayList();
        DictItemEntity dictItemEntity = new DictItemEntity();
        dictItemEntity.setDictCode("education");
        dictItemEntity.setItemLabel("小学");
        dictItemEntity.setItemValue("010");
        dictItemEntityList.add(dictItemEntity);

        DictItemEntity dictItemEntity2 = new DictItemEntity();
        dictItemEntity2.setDictCode("education");
        dictItemEntity2.setItemLabel("初中");
        dictItemEntity2.setItemValue("020");
        dictItemEntityList.add(dictItemEntity2);

        DictItemEntity dictItemEntity3 = new DictItemEntity();
        dictItemEntity3.setDictCode("education");
        dictItemEntity3.setItemLabel("高中/职中");
        dictItemEntity3.setItemValue("030");
        dictItemEntityList.add(dictItemEntity3);

        DictItemEntity dictItemEntity4 = new DictItemEntity();
        dictItemEntity4.setDictCode("education");
        dictItemEntity4.setItemLabel("大学专科");
        dictItemEntity4.setItemValue("040");
        dictItemEntityList.add(dictItemEntity4);

        DictItemEntity dictItemEntity5 = new DictItemEntity();
        dictItemEntity5.setDictCode("education");
        dictItemEntity5.setItemLabel("大学本科");
        dictItemEntity5.setItemValue("050");
        dictItemEntityList.add(dictItemEntity5);

        DictItemEntity dictItemEntity6 = new DictItemEntity();
        dictItemEntity6.setDictCode("education");
        dictItemEntity6.setItemLabel("研究生");
        dictItemEntity6.setItemValue("060");
        dictItemEntityList.add(dictItemEntity6);

        DictItemEntity dictItemEntity7 = new DictItemEntity();
        dictItemEntity7.setDictCode("education");
        dictItemEntity7.setItemLabel("硕士");
        dictItemEntity7.setItemValue("070");
        dictItemEntityList.add(dictItemEntity7);

        DictItemEntity dictItemEntity8 = new DictItemEntity();
        dictItemEntity8.setDictCode("education");
        dictItemEntity8.setItemLabel("博士");
        dictItemEntity8.setItemValue("080");
        dictItemEntityList.add(dictItemEntity8);

        DictItemEntity dictItemEntity9 = new DictItemEntity();
        dictItemEntity9.setDictCode("education");
        dictItemEntity9.setItemLabel("博士后");
        dictItemEntity9.setItemValue("090");
        dictItemEntityList.add(dictItemEntity9);

        DictItemEntity dictItemEntity10 = new DictItemEntity();
        dictItemEntity10.setDictCode("education");
        dictItemEntity10.setItemLabel("其它");
        dictItemEntity10.setItemValue("999");
        dictItemEntityList.add(dictItemEntity10);

        dictItemService.saveBatch(dictItemEntityList);
    }
}
