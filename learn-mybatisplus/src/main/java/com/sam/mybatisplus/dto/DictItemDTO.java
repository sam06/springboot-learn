package com.sam.mybatisplus.dto;

import lombok.*;

import java.io.Serializable;

/**
 * @classname: DictItemDTO
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/14 17:37
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DictItemDTO implements Serializable {
    /**
     * 字典标签(显示项)
     */
    private String itemLabel;
    /**
     * 字典键值
     */
    private String itemValue;
}
