package com.sam.mybatisplus.dto;

import com.sam.mybatisplus.support.annotation.ConvertDateTimeZone;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @classname: UserVo
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/10 15:29
 */
@Data
public class UserVo implements Serializable {
    private String username;

    @ConvertDateTimeZone
    private Date birthDay;

    AccountVo accountVo;

    List<AccountVo> accountVoList;

}
