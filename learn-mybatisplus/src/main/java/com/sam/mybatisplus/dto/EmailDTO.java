package com.sam.mybatisplus.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @classname: EmailDTO
 * @description: TODO
 * @Author: sam
 * @date: 2021/7/5 18:03
 */
@Data
public class EmailDTO implements Serializable {
    private String from;
    private String pwd;
    private String to;
    private String title;
    private String content;
}
