package com.sam.mybatisplus.dto;

import com.sam.mybatisplus.support.annotation.ConvertDateTimeZone;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @classname: OrderDTO
 * @description: 订单
 * @Author: sam
 * @date: 2021/4/25 16:33
 */
@Data
public class OrderDTO implements Serializable {

    private String orderNo;

    private String customerName;

    @ConvertDateTimeZone
    private Date createTime;

    @ConvertDateTimeZone
    private String LocalCreateTime;
}
