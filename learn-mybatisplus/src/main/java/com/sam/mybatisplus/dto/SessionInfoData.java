package com.sam.mybatisplus.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sam.mybatisplus.response.BaseResponse;
import com.sam.mybatisplus.support.annotation.ConvertDateTimeZone;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @classname: SessionInfoData
 * @description: session返回实体类
 * @author: Amber Chen
 * @date: 2020/11/05 09:59
 */
@Data
@ApiModel(value = "session返回实体类")
public class SessionInfoData extends BaseResponse implements Serializable {

    @ApiModelProperty(value = "课堂id")
    private String lessonId;

    @ApiModelProperty(value = "课堂编号")
    private String lessonNo;

    @ConvertDateTimeZone
    @ApiModelProperty(value = "开始时间")
    private String startTime;

    @ConvertDateTimeZone
    @ApiModelProperty(value = "结束时间")
    private String endTime;

    @ApiModelProperty(value = "课程时长（单位：小时）")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal duration;

    @ApiModelProperty(value = "教师id")
    private String tutorId;

    @ApiModelProperty(value = "教师")
    private String tutorName;

    @ApiModelProperty(value = "课堂号")
    private String roomNumber;

    @ApiModelProperty("课程类型（1-online；2-in person）")
    private String classType;

    @ApiModelProperty(value = "学科id")
    private String subjectId;

    @ApiModelProperty(value = "学科名称")
    private String subjectName;

    @ApiModelProperty(value = "学科等级")
    private String subjectLevel;

    @ApiModelProperty("sessionType名字")
    private String sessionTypeName;

    @ApiModelProperty("payGroup名字")
    private String payGroupName;

    @ApiModelProperty(value = "学科状态(-1-已过期;0-待开始;1-已提交;2-缺席;3-审核通过;4-已开发票;5-已支付（家长付的）;6-已结算(付给老师))")
    private String lessonStatus;

    @ApiModelProperty(value = "课程分类（1-中国/CN课程；2-英国/UK课程）")
    private String classification;

    @ApiModelProperty(value = "课程回放地址")
    private String lessonRecord;

    @ApiModelProperty(value = "是否显示报告按钮（0-否；1-是）")
    private String reportView;

    @ApiModelProperty(value = "学生信息")
    private List<SessionStudentData> students;
}
