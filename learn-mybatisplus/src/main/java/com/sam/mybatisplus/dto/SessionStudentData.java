package com.sam.mybatisplus.dto;

import com.sam.mybatisplus.response.BaseResponse;
import com.sam.mybatisplus.support.annotation.ConvertDateTimeZone;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @classname: SessionStudentData
 * @description: 课堂session学生信息
 * @author: Amber Chen
 * @date: 2020/11/05 10:50
 */
@Data
@ApiModel(value = "课堂session学生信息")
public class SessionStudentData extends BaseResponse {

    @ApiModelProperty(value = "学生id")
    private String studentId;

    @ApiModelProperty(value = "学生名字")
    private String studentName;

    @ApiModelProperty(value = "是否有课程报告")
    private boolean hasReport;

    @ConvertDateTimeZone
    private String createTime;
}
