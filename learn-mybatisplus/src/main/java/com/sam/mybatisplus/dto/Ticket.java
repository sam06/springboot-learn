package com.sam.mybatisplus.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @classname: Ticket
 * @description: TODO
 * @Author: sam
 * @date: 2021/4/13 18:28
 */
@Data
public class Ticket implements Serializable {
    private String ticketId;
    private String homesId;
    private String ticketCreateTime;
    private String ticketCompany;
    private String sysName;
    private String moneyLittle;
    private String moneyBig;
    private String accountCompany;
    private String bedNumber;
    private String username;
    private String password;
    List<AccountVo> accountVoList;
}
