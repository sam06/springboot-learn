package com.sam.mybatisplus.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @classname: LessonVo
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/21 15:14
 */
@Data
public class LessonVo implements Serializable {
    private String lessonName;

    private String type;

    private BigDecimal price;
}
