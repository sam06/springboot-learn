package com.sam.mybatisplus.dto;

import com.sam.mybatisplus.support.annotation.ConvertDateTimeZone;
import lombok.Data;

import java.io.Serializable;

/**
 * @classname: AddressVo
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/11 14:28
 */
@Data
public class AddressVo implements Serializable {
    private String addr;

    @ConvertDateTimeZone
    private String createTime;
}
