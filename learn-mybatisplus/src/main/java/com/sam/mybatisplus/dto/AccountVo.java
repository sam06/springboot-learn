package com.sam.mybatisplus.dto;

import com.sam.mybatisplus.support.annotation.ConvertDateTimeZone;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @classname: AccountVo
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/10 15:30
 */
@Data
public class AccountVo implements Serializable {

    private String accountName;

    @ConvertDateTimeZone
    private String startTime;

    @ConvertDateTimeZone
    private Date createTime;
}
