package com.sam.mybatisplus.dto;

import com.sam.mybatisplus.support.annotation.ConvertDateTimeZone;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @classname: OrderVO
 * @description: TODO
 * @Author: sam
 * @date: 2021/4/27 17:48
 */
@Data
public class OrderVO implements Serializable {
    @ConvertDateTimeZone(pattern ="yyyy-MM-dd")
    private String createTime;
    @ConvertDateTimeZone(pattern ="yyyy-MM-dd HH:mm:ss")
    private String updateTime;

    @ConvertDateTimeZone(pattern ="HH:mm:ss")
    private String startTime;

    @ConvertDateTimeZone(pattern ="yyyy-MM-dd")
    private Date curDate;
}
