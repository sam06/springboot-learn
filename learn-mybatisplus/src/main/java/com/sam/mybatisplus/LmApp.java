package com.sam.mybatisplus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @classname: App
 * @description: TODO
 * @Author: sam
 * @date: 2021/3/26 14:29
 */
@EnableAspectJAutoProxy(proxyTargetClass=true)
@MapperScan("com.sam.mybatisplus.mapper")
@SpringBootApplication
public class LmApp {
    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    RedisTemplate redisTemplate;

    public static void main(String[] args) {
        SpringApplication.run(LmApp.class,args);
    }

}
