package com.sam.mybatisplus.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @classname: JsonSerializeConfig
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/14 18:08
 */
@Configuration
public class JsonSerializeConfig extends WebMvcConfigurationSupport {

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

}
