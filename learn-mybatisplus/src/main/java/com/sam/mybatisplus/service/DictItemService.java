package com.sam.mybatisplus.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.mybatisplus.dto.DictItemDTO;
import com.sam.mybatisplus.entity.DictItemEntity;

import java.util.List;

/**
 * 系统字典条目表
 *
 * @author sam
 * @email sam@163.com
 * @date 2021-05-13 19:03:27
 */
public interface DictItemService extends IService<DictItemEntity> {
    /**
     * 根据DictCode获取字典项列表
            
     * @Author: sam
     * @Date: 2021/5/14 17:39
     **/
    List<DictItemDTO> getListByDictCode(String dictCode);
}

