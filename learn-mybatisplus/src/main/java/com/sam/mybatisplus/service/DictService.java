package com.sam.mybatisplus.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.mybatisplus.entity.DictEntity;

/**
 * 系统字典表
 *
 * @author sam
 * @email sam@163.com
 * @date 2021-05-13 19:03:27
 */
public interface DictService extends IService<DictEntity> {

}

