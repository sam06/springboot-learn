package com.sam.mybatisplus.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.mybatisplus.entity.DictEntity;
import com.sam.mybatisplus.mapper.DictMapper;
import com.sam.mybatisplus.service.DictService;
import org.springframework.stereotype.Service;

@Service("dictService")
public class DictServiceImpl extends ServiceImpl<DictMapper, DictEntity> implements DictService {

}