package com.sam.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.mybatisplus.entity.Orders;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author sam
 * @since 2021-03-28
 */
public interface OrdersService extends IService<Orders> {

}
