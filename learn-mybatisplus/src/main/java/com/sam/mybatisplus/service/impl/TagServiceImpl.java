package com.sam.mybatisplus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.mybatisplus.entity.Tag;
import com.sam.mybatisplus.mapper.TagMapper;
import com.sam.mybatisplus.service.TagService;
import org.springframework.stereotype.Service;

/**
 * @classname: TagServiceImpl
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/15 11:51
 */
@Service
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag> implements TagService {
}
