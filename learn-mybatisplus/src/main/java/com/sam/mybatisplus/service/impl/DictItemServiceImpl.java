package com.sam.mybatisplus.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.mybatisplus.dto.DictItemDTO;
import com.sam.mybatisplus.entity.DictItemEntity;
import com.sam.mybatisplus.mapper.DictItemMapper;
import com.sam.mybatisplus.service.DictItemService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("dictItemService")
public class DictItemServiceImpl extends ServiceImpl<DictItemMapper, DictItemEntity> implements DictItemService {

    /**
     * 根据DictCode获取字典项列表
     *
     * @param dictCode
     * @Author: sam
     * @Date: 2021/5/14 17:39
     */
    @Override
    public List<DictItemDTO> getListByDictCode(String dictCode) {
        LambdaQueryWrapper<DictItemEntity> lqw = Wrappers.lambdaQuery();
        List<DictItemEntity> dictItemEntityList = this.baseMapper.selectList(lqw);
        List<DictItemDTO> dictItemDTOList = dictItemEntityList.stream().map(item -> {
            DictItemDTO dictItemDTO = DictItemDTO.builder()
                    .itemLabel(item.getItemLabel())
                    .itemValue(item.getItemValue())
                    .build();

            return dictItemDTO;
        }).collect(Collectors.toList());
        return dictItemDTOList;
    }
}