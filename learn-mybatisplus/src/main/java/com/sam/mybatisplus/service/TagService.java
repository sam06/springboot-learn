package com.sam.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.mybatisplus.entity.Tag;

/**
 * @classname: TagService
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/15 11:49
 */
public interface TagService extends IService<Tag> {
}
