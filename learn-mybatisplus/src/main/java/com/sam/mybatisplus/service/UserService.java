package com.sam.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.mybatisplus.entity.User;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author sam
 * @since 2021-03-28
 */
public interface UserService extends IService<User> {

}
