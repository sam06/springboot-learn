package com.sam.mybatisplus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.mybatisplus.entity.Orderdetail;
import com.sam.mybatisplus.mapper.OrderdetailMapper;
import com.sam.mybatisplus.service.OrderdetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单明细表 服务实现类
 * </p>
 *
 * @author sam
 * @since 2021-03-28
 */
@Service
public class OrderdetailServiceImpl extends ServiceImpl<OrderdetailMapper, Orderdetail> implements OrderdetailService {

}
