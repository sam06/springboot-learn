package com.sam.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.mybatisplus.entity.Orders;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author sam
 * @since 2021-03-28
 */
public interface OrdersMapper extends BaseMapper<Orders> {

}
