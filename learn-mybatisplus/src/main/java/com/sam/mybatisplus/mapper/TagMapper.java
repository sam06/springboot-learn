package com.sam.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.mybatisplus.entity.Tag;

/**
 * @classname: TagMapper
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/15 11:43
 */
public interface TagMapper extends BaseMapper<Tag> {

}
