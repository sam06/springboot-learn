package com.sam.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.mybatisplus.entity.DictItemEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统字典条目表
 * 
 * @author sam
 * @email sam@163.com
 * @date 2021-05-13 19:03:27
 */
@Mapper
public interface DictItemMapper extends BaseMapper<DictItemEntity> {
	
}
