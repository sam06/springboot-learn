package com.sam.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.mybatisplus.entity.Items;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author sam
 * @since 2021-03-28
 */
public interface ItemsMapper extends BaseMapper<Items> {

}
