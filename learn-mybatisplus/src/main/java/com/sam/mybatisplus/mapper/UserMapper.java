package com.sam.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.mybatisplus.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author sam
 * @since 2021-03-28
 */
public interface UserMapper extends BaseMapper<User> {

    int batchUpdate(@Param("userList") List<User> userList);
}
