package com.sam.mybatisplus.controller;

import com.sam.common.Result;
import com.sam.mybatisplus.component.PdfProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @classname: TestController
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/16 9:58
 */
@RestController
public class TestController {

    @Autowired
    PdfProperties pdfProperties;

    @GetMapping("getFooter")
    public Result<String> getFooter(){
        return Result.ok(pdfProperties.getFooter());
    }
}
