package com.sam.mybatisplus.controller;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.sam.common.Result;
import com.sam.mybatisplus.dto.*;
import com.sam.mybatisplus.support.PageResult;
import com.sam.mybatisplus.support.annotation.ConvertDateTimeZone;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @classname: OrderController
 * @description: TODO
 * @Author: sam
 * @date: 2021/4/23 15:33
 */
@Slf4j
@RequestMapping("order")
@RestController
public class OrderController {
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
    @RequestMapping("convert")
    public String convertDate(String date){
        return date;
    }

    @GetMapping("list")
    public Result<List<OrderDTO>> list(@NotNull @RequestParam @ConvertDateTimeZone(pattern = "yyyy-MM-dd HH:mm:ss") String createTime, @ConvertDateTimeZone(pattern = "yyyy-MM-dd HH:mm:ss") Date curDate){
        log.info("OrderController create time:{},date:{}",createTime,curDate);
        return Result.ok(getOrders());
    }

    @GetMapping("page")
    public Result<List<OrderDTO>> page(OrderVO orderVO){
        log.info("OrderController page orderVO:{}", JSON.toJSONString(orderVO));
        return Result.ok(getOrders());
    }

    @GetMapping("query")
    public Result<List<OrderDTO>> query(){
        return Result.ok(getOrders());
    }

    @GetMapping("get")
    public Result<PageResult<OrderDTO>> get(){
        OrderDTO dto = new OrderDTO();
        dto.setOrderNo(UUID.randomUUID().toString());
        dto.setLocalCreateTime(DateUtil.today());
        log.debug("返回dto:{}", JSON.toJSONString(dto));

        List<OrderDTO> orders = getOrders();
        PageResult pageResult = new PageResult(orders,Long.parseLong(orders.size()+""),1,10);

        return Result.ok(pageResult);
    }

    @GetMapping("getNull")
    public Result<OrderDTO> getNull(){
        return Result.ok(null);
    }

    private List<OrderDTO> getOrders(){
        List<OrderDTO> dtoList = new ArrayList<>();
        for (int i=0;i<10;i++){
            OrderDTO dto = new OrderDTO();
            dto.setOrderNo(UUID.randomUUID().toString());
            dto.setCustomerName("test"+i);
            dto.setCreateTime(new Date());
            dto.setLocalCreateTime(DateUtil.now());
            dtoList.add(dto);
        }
        return dtoList;
    }

    @PostMapping
    public Result add(@RequestBody OrderVO orderVO){
        return Result.ok(orderVO);
    }

    @PostMapping("user")
    public Result<List<OrderDTO>> user(@RequestBody UserVo userVo){
        log.info("userVo:{}",JSON.toJSONString(userVo));
        return Result.ok(getOrders());
    }

    @GetMapping("user2")
    public Result<String> user2(@ConvertDateTimeZone String beginTime){
        log.info("user2,beginTime:{}",beginTime);
        /*UserVo userVo = new UserVo();
        userVo.setUsername("hello");
        AccountVo accountVo = new AccountVo();
        accountVo.setEndTime(DateTime.now().toString());
        userVo.setAccountVo(accountVo);*/

        @ConvertDateTimeZone
        String result=DateUtil.today();

        @ConvertDateTimeZone
        Date date = new Date();
        return Result.ok(date);
    }


    @GetMapping("getUser")
    public Result<UserVo> getUser(){
        UserVo userVo = new UserVo();
        userVo.setBirthDay(DateTime.now());

        AccountVo accountVo = new AccountVo();
        accountVo.setAccountName("user");
        accountVo.setStartTime("2021-05-11 10:00:00");

        AccountVo accountVo2 = new AccountVo();
        accountVo2.setAccountName("admin");
        accountVo2.setStartTime("2021-05-11 11:00:00");

        AccountVo accountVo3 = new AccountVo();
        accountVo3.setAccountName("sa");
        accountVo3.setStartTime("2021-05-11 12:00:00");

        List<AccountVo> accountVoList = new ArrayList<>();
        accountVoList.add(accountVo2);
        accountVoList.add(accountVo3);

        userVo.setAccountVo(accountVo);

        userVo.setAccountVoList(accountVoList);
        return Result.ok(userVo);
    }


    @GetMapping("pageResult")
    public Result<PageResult<SessionInfoData>> pageResult(){
        SessionStudentData sessionStudentData = new SessionStudentData();
        sessionStudentData.setStudentName("hello");
        sessionStudentData.setCreateTime("2021-05-01 12:00:00");

        List<SessionStudentData> sessionStudentDataList = new ArrayList<>();
        sessionStudentDataList.add(sessionStudentData);

        SessionInfoData sessionInfoData = new SessionInfoData();
        sessionInfoData.setStartTime("2021-05-11 12:00:00");
        sessionInfoData.setEndTime("2021-05-10 12:00:00");

        sessionInfoData.setStudents(sessionStudentDataList);

        List<SessionInfoData> list = new ArrayList<>();
        list.add(sessionInfoData);
        PageResult pageResult = new PageResult(list,0L,1,1);

        return Result.ok(pageResult);
    }
}
