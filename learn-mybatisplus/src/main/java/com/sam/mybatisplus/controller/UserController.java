package com.sam.mybatisplus.controller;

import com.sam.common.Result;
import com.sam.mybatisplus.entity.User;
import com.sam.mybatisplus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @classname: UserController
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/14 18:22
 */
@RequestMapping("user")
@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("{id}")
    public Result<User> get(@PathVariable("id")String id){
        User user = userService.getById(id);
        return Result.ok(user);
    }
}
