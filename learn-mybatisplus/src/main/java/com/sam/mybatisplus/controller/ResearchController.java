package com.sam.mybatisplus.controller;

import com.sam.common.Result;
import com.sam.mybatisplus.support.annotation.Authorization;
import com.sam.mybatisplus.support.component.Context;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @classname: ResearchController
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/3 14:30
 */
@Slf4j
@RestController
@RequestMapping("test")
public class ResearchController {

    @Autowired
    Context context;

    @GetMapping("getTicket")
    public Result getTicket(String type){
        return Result.ok(context.getTicketList(type));
    }

    @Authorization(businessType = "6")
    @PostMapping("/get")
    public Result get(String id){
        log.info("id:{},sourceTypes:{}", id);
        return Result.ok();
    }

    @PostMapping("/get1")
    public Result get1(String name,Integer age){
        return Result.ok();
    }
}
