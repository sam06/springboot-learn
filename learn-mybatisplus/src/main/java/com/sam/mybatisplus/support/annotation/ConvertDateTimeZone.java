package com.sam.mybatisplus.support.annotation;

import java.lang.annotation.*;

/**
 * @classname: ConvertDateTimeZone
 * @description: 转换日期到指定时间
 * @Author: sam
 * @date: 2021/4/27 11:18
 */
@Target({ElementType.PARAMETER, ElementType.FIELD,ElementType.LOCAL_VARIABLE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ConvertDateTimeZone {
    String pattern() default "yyyy-MM-dd HH:mm:ss";
}
