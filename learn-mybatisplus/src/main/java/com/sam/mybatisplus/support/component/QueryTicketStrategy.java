package com.sam.mybatisplus.support.component;

/**
 * @classname: QueryTicketStrategy
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/7 16:28
 */
public interface QueryTicketStrategy {
    String getTicketList();
}
