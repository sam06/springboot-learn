package com.sam.mybatisplus.support.aspect;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @classname: DictJsonSerializer
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/14 17:00
 */
@Slf4j
@Configuration
public class DictJsonSerializer extends JsonSerializer<Object> {
    @Override
    public void serialize(Object obj, JsonGenerator generator, SerializerProvider serializerProvider) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        StringBuffer dictValName = new StringBuffer("");
        String currentName = generator.getOutputContext().getCurrentName();
        log.info("currentName:{}",currentName);
    }
}
