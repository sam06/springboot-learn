package com.sam.mybatisplus.support.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @classname: Dict
 * @description: 字典注解
 * @Author: sam
 * @date: 2021/5/14 16:21
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DictConvert {
    /**
     * 转换的类型
     * content : 统计内容转换
     */
    String type() default "";

    /**
     * 1 : {"code":"code","name":"name"}  默认
     * 2 : {"code":"name"}
     */
    int trans() default 1;
}
