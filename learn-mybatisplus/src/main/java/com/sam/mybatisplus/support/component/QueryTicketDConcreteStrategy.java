package com.sam.mybatisplus.support.component;

import org.springframework.stereotype.Service;

/**
 * @classname: QueryTicketDConcreteStrategy
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/7 16:30
 */
@Service("D")
public class QueryTicketDConcreteStrategy implements QueryTicketStrategy{
    @Override
    public String getTicketList() {
        return "火车";
    }
}
