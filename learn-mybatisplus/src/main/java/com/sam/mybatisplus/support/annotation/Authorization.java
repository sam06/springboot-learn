package com.sam.mybatisplus.support.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @classname: Authorization
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/3 14:25
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Authorization {
    /**
     * 业务代码
     * @return
     */
    String[] businessType();
}
