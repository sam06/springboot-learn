package com.sam.mybatisplus.support.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * @classname: ChangeDateForTimeZoneAspect
 * @description: TODO
 * @Author: sam
 * @date: 2021/4/28 16:47
 */
@Aspect
@Component
public class ChangeDateForTimeZoneAspect {
    /*@Autowired
    private UserInfoService userInfoService;

    @Resource(name = "redisTemplateObject")
    private RedisTemplate<Object,Object> redisTemplate;

    private Object changObjectValue(Object _obj, Set<String> propertySet,
                                    String timeZoneId) throws Exception {
        Class<?> resultClz = _obj.getClass();
        for(String property:propertySet){
            Field field = getField(resultClz, property);
            if(field!=null){
                //权限，允许修改
                field.setAccessible(true);
                //获得数据库中的属性值
                Object fieldValue = field.get(_obj);
                if(StringUtils.isNotBlank(ObjectUtils.toString(fieldValue))){
                    //根据用户时区转换后的时间
                    Date date = DateUtils.timeZoneTransfer((Date)fieldValue, 8, Integer.valueOf(timeZoneId));
                    String formatDate = DateUtils.format(date, "yyyy-MM-dd HH:mm:ss");
                    //反射修改对象属性
                    setField(resultClz,field,_obj,formatDate);
                }
            }
        }
        return _obj;
    }

    *//*
        反射获取对象
        若当前对象没有field属性，向父类找，直到没有父类
     *//*
    public Field getField(Class<?> clazz,String field){
        //没有父类时结束
        if (clazz == null){
            return null;
        }
        try {
            return clazz.getDeclaredField(field);
        } catch (NoSuchFieldException e) {
            //方法重载
            return getField(clazz.getSuperclass(), field);
        }
    }

    *//*
        反射修改对象属性
        set 父类只找一次
     *//*
    public Field setField(Class<?> clazz,Field field,Object _obj,String formatDate) throws Exception{
        //验证参数
        if (clazz == null){
            return null;
        }
        try {
            Field f = clazz.getDeclaredField(field.getName() + "Fmt");
            f.setAccessible(true);
            f.set(_obj,formatDate);
            return f;
        } catch (NoSuchFieldException e) {
            Field f = clazz.getSuperclass().getDeclaredField(field.getName() + "Fmt");
            f.setAccessible(true);
            f.set(_obj,formatDate);
            return f;
        }
    }

    @Around(value = "@annotation(com.sendcloud.marketing.api.util.HandleDateField)")
    public Object hanle(ProceedingJoinPoint joinPoint) throws Throwable {

        MethodSignature methodSignature = (MethodSignature) joinPoint
                .getSignature();
        Method method = methodSignature.getMethod();
        HandleDateField ananHandleDateField = method
                .getAnnotation(HandleDateField.class);

        String[] propertys = ananHandleDateField.dateFiled();
        Set<String> propertySet = new HashSet<String>();
        if (propertys != null) {
            for (String e : propertys) {

                propertySet.add(e);
            }

        }

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
        Integer userId =(Integer) redisTemplate.opsForValue().get(BaseController.CONTACT_API_TOKEN_PREFIX + request.getHeader("token"));
        UserInfo userInfo = userInfoService.findUserInfo(userId);

        Object returnValue = joinPoint.proceed();
        if(StringUtils.isEmpty(userInfo.getTimeZone())){
            return returnValue;
        }

        //R为自定义的返回对象
        if (returnValue instanceof R) {
            R r = (R) returnValue;
            Map<String, ?> map = r.getData();
            for (Object obj : map.values()) {
                //若接口返回的数据类型为对象
                if(obj instanceof Object){
                    changObjectValue(obj,propertySet,userInfo.getTimeZone());
                }
                //若接口返回的数据类型为集合类型
                if (obj instanceof List) {
                    List<Object> objList = (List<Object>) obj;
                    for (Object _obj : objList) {
                        changObjectValue(_obj,propertySet,userInfo.getTimeZone());
                    }
                }
            }

        }

        return returnValue;
    }*/

}
