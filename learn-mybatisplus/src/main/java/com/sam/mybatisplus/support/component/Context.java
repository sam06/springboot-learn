package com.sam.mybatisplus.support.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @classname: Context
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/7 16:31
 */
@Service
public class Context {
    @Autowired
    private Map<String, QueryTicketStrategy> map = new HashMap<>();

    public String getTicketList(String type) {
        return map.get(type).getTicketList();
    }
}
