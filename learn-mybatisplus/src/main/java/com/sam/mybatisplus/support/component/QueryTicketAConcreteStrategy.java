package com.sam.mybatisplus.support.component;

import org.springframework.stereotype.Service;

/**
 * @classname: QueryTicketAService
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/7 16:29
 */
@Service("A")
public class QueryTicketAConcreteStrategy implements QueryTicketStrategy{
    @Override
    public String getTicketList() {
        return "高铁";
    }
}
