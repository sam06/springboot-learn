package com.sam.mybatisplus.component;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @classname: Pdf
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/16 10:03
 */
@Data
@Component
@ConfigurationProperties(prefix = "pdf")
public class PdfProperties {
    private String footer;
}
