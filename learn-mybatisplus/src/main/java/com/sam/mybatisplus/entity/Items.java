package com.sam.mybatisplus.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author sam
 * @since 2021-03-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Items extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品定价
     */
    private Float price;

    /**
     * 商品描述
     */
    private String detail;

    /**
     * 商品图片
     */
    private String pic;

}
