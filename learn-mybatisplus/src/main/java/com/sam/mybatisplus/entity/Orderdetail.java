package com.sam.mybatisplus.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 订单明细表
 * </p>
 *
 * @author sam
 * @since 2021-03-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Orderdetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 订单id
     */
    private Integer ordersId;

    /**
     * 商品id
     */
    private Integer itemsId;

    /**
     * 商品购买数量
     */
    private Integer itemsNum;

}
