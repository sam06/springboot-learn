package com.sam.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sam.mybatisplus.support.annotation.DictConvert;
import com.sam.mybatisplus.support.aspect.DictJsonSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author sam
 * @since 2021-03-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_user")
public class User extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名称
     */
    private String username;

    /**
     * 生日
     */
    private LocalDate birthday;

    /**
     * 性别
     */
    @JsonSerialize(using = DictJsonSerializer.class)
    @DictConvert(type = "gender")
    private String gender;

    /**
     * 地址
     */
    private String address;


}
