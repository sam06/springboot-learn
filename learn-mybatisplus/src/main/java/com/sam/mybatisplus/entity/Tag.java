package com.sam.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @classname: Tag
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/15 11:40
 */
@Data
@TableName("tb_tag")
public class Tag {
    private Integer id;

    private String tagName;
}
