package com.sam.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统字典条目表
 * 
 * @author sam
 * @email sam@163.com
 * @date 2021-05-13 19:03:27
 */
@Data
@TableName("sys_dict_item")
public class DictItemEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键(雪花算法id)
	 */
	@TableId
	private Long id;
	/**
	 * 字典标识
	 */
	private String dictCode;
	/**
	 * 字典标签(显示项)
	 */
	private String itemLabel;
	/**
	 * 字典键值
	 */
	private String itemValue;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 创建人
	 */
	private String createUser;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 
	 */
	private String updateUser;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除标志0有效，1无效
	 */
	private Integer isDeleted;

}
