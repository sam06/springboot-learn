package com.sam.mybatisplus.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author sam
 * @since 2021-03-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Orders extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 下单用户id
     */
    private Integer userId;

    /**
     * 订单号
     */
    private String number;

    /**
     * 备注
     */
    private String note;

}
