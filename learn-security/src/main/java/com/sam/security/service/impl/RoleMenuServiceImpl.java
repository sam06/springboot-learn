package com.sam.security.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.security.dao.RoleMenuDao;
import com.sam.security.entity.RoleMenuEntity;
import com.sam.security.service.RoleMenuService;
import org.springframework.stereotype.Service;

@Service("roleMenuService")
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuDao, RoleMenuEntity> implements RoleMenuService {

}