package com.sam.security.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.security.entity.MenuEntity;

import java.util.List;

/**
 * 
 *
 * @author sam
 * @email sam@163.com
 * @date 2021-04-17 17:53:00
 */
public interface MenuService extends IService<MenuEntity> {
    /**
     * 根据角色集合，查询权限列表
     * @param roleCodes
     * @return
     */
    List<String> queryMenuByRoleCods(List<String> roleCodes);;
}

