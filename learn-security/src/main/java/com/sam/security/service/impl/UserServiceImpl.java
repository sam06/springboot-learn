package com.sam.security.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.security.dao.UserDao;
import com.sam.security.entity.UserEntity;
import com.sam.security.service.UserService;
import org.springframework.stereotype.Service;

/**
 * 用户service
 * @author sam
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {


    /**
     * 根据用户名查询用户
     *
     * @param username
     * @return
     */
    @Override
    public UserEntity queryByUserName(String username) {
        LambdaQueryWrapper<UserEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserEntity::getAccountCode,username);
        return baseMapper.selectOne(queryWrapper);
    }
}