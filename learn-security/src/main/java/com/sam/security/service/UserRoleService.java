package com.sam.security.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.security.entity.UserRoleEntity;

/**
 * 
 *
 * @author sam
 * @email sam@163.com
 * @date 2021-04-17 17:53:00
 */
public interface UserRoleService extends IService<UserRoleEntity> {

}

