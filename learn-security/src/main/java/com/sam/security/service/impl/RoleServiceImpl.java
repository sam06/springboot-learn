package com.sam.security.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.security.dao.RoleDao;
import com.sam.security.entity.RoleEntity;
import com.sam.security.service.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author sam
 */
@Service("roleService")
public class RoleServiceImpl extends ServiceImpl<RoleDao, RoleEntity> implements RoleService {


    /**
     * 查询角色列表
     *
     * @param username
     * @return
     */
    @Override
    public List<String> queryByUserName(String username) {
        return baseMapper.queryByUserName(username);
    }
}