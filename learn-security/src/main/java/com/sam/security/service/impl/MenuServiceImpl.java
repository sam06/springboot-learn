package com.sam.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.security.dao.MenuDao;
import com.sam.security.entity.MenuEntity;
import com.sam.security.service.MenuService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 菜单Service
 * @author sam
 */
@Service("menuService")
public class MenuServiceImpl extends ServiceImpl<MenuDao, MenuEntity> implements MenuService {


    /**
     * 根据角色集合，查询权限列表
     *
     * @param roleCodes
     * @return
     */
    @Override
    public List<String> queryMenuByRoleCods(List<String> roleCodes) {
        return baseMapper.queryMenuByRoleCods(roleCodes);
    }
}