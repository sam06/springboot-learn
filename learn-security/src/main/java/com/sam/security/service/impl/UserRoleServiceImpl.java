package com.sam.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sam.security.dao.UserRoleDao;
import com.sam.security.entity.UserRoleEntity;
import com.sam.security.service.UserRoleService;
import org.springframework.stereotype.Service;


@Service("userRoleService")
public class UserRoleServiceImpl extends ServiceImpl<UserRoleDao, UserRoleEntity> implements UserRoleService {


}