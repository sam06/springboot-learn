package com.sam.security.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.security.entity.RoleEntity;

import java.util.List;

/**
 * 
 *
 * @author sam
 * @email sam@163.com
 * @date 2021-04-17 17:53:00
 */
public interface RoleService extends IService<RoleEntity> {

    /**
     * 查询角色列表
     * @param username
     * @return
     */
    List<String> queryByUserName(String username);
}

