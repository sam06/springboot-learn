package com.sam.security.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sam.security.entity.UserEntity;

/**
 * 
 *
 * @author sam
 * @email sam@163.com
 * @date 2021-04-17 17:53:00
 */
public interface UserService extends IService<UserEntity> {
    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    UserEntity queryByUserName(String username);
}

