package com.sam.security;

import com.sam.security.config.Person;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @classname: SecApp
 * @description: TODO
 * @Author: sam
 * @date: 2021/3/29 16:16
 */
@MapperScan(basePackages="com.sam.security.dao")
@SpringBootApplication
public class SecApp {
    @Autowired
    Person person;
    public static void main(String[] args) {
        SpringApplication.run(SecApp.class,args);
        new SecApp().get();
    }

    public void get(){
        System.out.println(person);
    }
}
