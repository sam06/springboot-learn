package com.sam.security.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @classname: Person
 * @description: TODO
 * @Author: sam
 * @date: 2021/4/19 11:55
 */
@Data
@Configuration
@ConfigurationProperties("person")
public class Person {
    private List<String> city;
}
