package com.sam.security.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @classname: WebAppConfigurer
 * @description: TODO
 * @Author: sam
 * @date: 2021/4/1 14:00
 */
@Configuration
public class WebAppConfigurer implements WebMvcConfigurer {
    /**
     * 默认Url根路径跳转到/login，此url为spring security提供
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {

        //url根路径跳转到/login-view
        registry.addViewController("/login").setViewName("login");
        //registry.addViewController("/toLogin").setViewName("redirect:/login");
        // /login-view定位到自定义登录页面
        //registry.addViewController("/login-view").setViewName("/user/login");
    }
}
