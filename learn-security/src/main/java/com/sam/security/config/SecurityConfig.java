package com.sam.security.config;

import com.sam.security.component.CustomUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @classname: SecurityConfig
 * @description: TODO
 * @Author: sam
 * @date: 2021/3/29 16:55
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    CustomUserDetailService customUserDetailService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .formLogin()    // 开启httpbaseic认证
                    .loginPage("/login")
                    .loginProcessingUrl("/doLogin")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .defaultSuccessUrl("/")
                .and()
                .authorizeRequests()
                    .antMatchers("/login.html","/login","/doLogin").permitAll()
                .anyRequest().access("@rbacService.hasPermisson(request,authentication)");
                    /*.antMatchers("/","/biz1","/biz2")
                        .hasAnyAuthority("ROLE_dev","ROLE_sa")
                        //.hasAnyRole("dev","sa") // 等价于同上面
                    .antMatchers("/syslog","/sysuser")
                        .hasAnyRole("sa")
                        *//*.antMatchers("/syslog").hasAuthority("sys:log")
                        .antMatchers("/sysuser").hasAuthority("sys:user")*//*
                .anyRequest()
                .authenticated();// 所有请求都需要访问登录认证才能访问*/
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web)  {
        // 将项目中静态资源路径开放出来
        web.ignoring().antMatchers("/config/**", "/css/**", "/fonts/**", "/img/**", "/js/**");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        /*auth.inMemoryAuthentication()
                .withUser("user")
                .password(passwordEncoder().encode("111111"))
                    .roles("dev")
                .and()
                .withUser("admin")
                .password(passwordEncoder().encode("admin"))
                    //.authorities("sys:log","sys:user")
                    .roles("sa")
                .and()
                .passwordEncoder(passwordEncoder());*/
        auth.userDetailsService(customUserDetailService).passwordEncoder(passwordEncoder());
    }
}
