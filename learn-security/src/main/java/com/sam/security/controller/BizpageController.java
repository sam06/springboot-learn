package com.sam.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @classname: BizpageController
 * @description: TODO
 * @Author: sam
 * @date: 2021/3/29 16:38
 */
@Controller
public class BizpageController {

    /**
     * 系统日志
     * @return
     */
    @GetMapping("/syslog")
    public String showLog(){
        return "syslog";
    }

    /**
     * 用户管理
     * @return
     */
    @GetMapping("/sysuser")
    public String showUser(){
        return "sysuser";
    }

    /**
     * 具体业务一
     * @return
     */
    @GetMapping("/biz1")
    public String biz1(){
        return "biz1";
    }
    /**
     * 具体业务二
     * @return
     */
    @GetMapping("/biz2")
    public String biz2(){
        return "biz2";
    }

}
