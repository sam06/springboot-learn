package com.sam.security.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.security.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 
 * 
 * @author sam
 * @email sam@163.com
 * @date 2021-04-17 17:53:00
 */
@Repository
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
	
}
