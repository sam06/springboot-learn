package com.sam.security.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.security.entity.RoleEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 
 * 
 * @author sam
 * @email sam@163.com
 * @date 2021-04-17 17:53:00
 */
@Mapper
public interface RoleDao extends BaseMapper<RoleEntity> {
    /**
     * 根据username获取角色code列表
     * @param username
     * @return
     */
    List<String> queryByUserName(String username);
}
