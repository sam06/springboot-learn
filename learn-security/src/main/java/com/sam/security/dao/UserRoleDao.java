package com.sam.security.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.security.entity.UserRoleEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author sam
 * @email sam@163.com
 * @date 2021-04-17 17:53:00
 */
@Mapper
public interface UserRoleDao extends BaseMapper<UserRoleEntity> {
	
}
