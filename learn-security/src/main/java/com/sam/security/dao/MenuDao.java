package com.sam.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sam.security.entity.MenuEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author sam
 * @email sam@163.com
 * @date 2021-04-17 17:53:00
 */
@Mapper
public interface MenuDao extends BaseMapper<MenuEntity> {
    /**
     * 根据角色集合，获取权限 列表
     * @param roleCodes
     * @return
     */
    List<String> queryMenuByRoleCods(@Param("roleCodes") List<String> roleCodes);
}
