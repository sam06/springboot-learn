package com.sam.security.component;


import com.sam.security.dto.UserDetail;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * @classname: RbacService
 * @description: TODO
 * @Author: sam
 * @date: 2021/4/18 19:38
 */
@Component("rbacService")
public class RbacService {
    public boolean hasPermisson(HttpServletRequest request, Authentication authentication){
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetail){
            UserDetail userDetail = (UserDetail) principal;

            String requestURI = request.getRequestURI();
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(requestURI);

            Collection<? extends GrantedAuthority> authorities = userDetail.getAuthorities();

            return authorities.contains(authority);
        }
        return false;
    }
}
