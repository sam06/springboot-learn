package com.sam.security;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @classname: Person
 * @description: TODO
 * @Author: sam
 * @date: 2021/4/2 14:49
 */
@Data
public class Person implements Serializable {
    private List<Student> students;
}
