package com.sam.security;

import lombok.Data;

import java.io.Serializable;

/**
 * @classname: Student
 * @description: TODO
 * @Author: sam
 * @date: 2021/4/2 14:49
 */
@Data
public class Student implements Serializable {
    private String id;
}
