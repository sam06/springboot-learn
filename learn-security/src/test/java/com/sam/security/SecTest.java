package com.sam.security;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @classname: SecTest
 * @description: TODO
 * @Author: sam
 * @date: 2021/3/29 17:56
 */
public class SecTest {

    @Test
    public void test(){
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String pwd="111111";
        String encode = passwordEncoder.encode(pwd);
        System.out.println("admin encode:"+encode);
        System.out.println("encode length:"+encode.length());

        String pwdEncode="$2a$10$BWjZmv9I4oae9EKYpRD6ce0ZtRVPmHy46YAUD9Lyi.S1ByYG4Ih72";

        boolean matches = passwordEncoder.matches(pwd, pwdEncode);
        System.out.println("是否匹配："+matches);
    }

    @Test
    public void test1(){
        System.out.println(LocalTime.MIN.plus(Duration.ofMinutes(30L)));

        BigDecimal min= new BigDecimal("90");
        BigDecimal bigDecimal = new BigDecimal(45);
        // 定义精确到小数
        System.out.println(min.divide(bigDecimal,2,BigDecimal.ROUND_HALF_UP));

        BigDecimal hour= new BigDecimal("20.5");
        DecimalFormat df1 = new DecimalFormat("#.00");
        System.out.println(df1.format(hour));// 2.20
        System.out.println(df1.format(2.246));// 2.25
    }



    @Test
    public void test2(){
        List<Student> studentList = new ArrayList<>();
        for (int i=0;i<3;i++){
            Student student = new Student();
            student.setId(i+"A");
            studentList.add(student);
        }
        List<Student> studentList2 = new ArrayList<>();
        for (int i=0;i<3;i++){
            Student student = new Student();
            student.setId(i+"B");
            studentList2.add(student);
        }
        Person person = new Person();
        person.setStudents(studentList);

        Person person2 = new Person();
        person2.setStudents(studentList2);

        List<Person> personList = new ArrayList<>();
        personList.add(person);
        personList.add(person2);

        List<String> collect = personList.stream().map(item -> {
            return item.getStudents().stream().map(student -> student.getId()).collect(Collectors.joining(","));
        }).collect(Collectors.toList());

        System.out.println(collect);
    }

}
