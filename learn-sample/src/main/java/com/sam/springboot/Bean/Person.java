package com.sam.springboot.Bean;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @classname: Person
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/25 21:57
 */
@Data
public class Person implements Serializable {
    private String name;

    private int age;

    List<Address> address;
}
