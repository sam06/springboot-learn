package com.sam.springboot.Bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @classname: Address
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/25 21:55
 */
@Data
public class AddressDTO implements Serializable {
    private String addNo;
    private String addName;
}
