package com.sam.springboot.Bean;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @classname: PersonDTO
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/1 14:47
 */
@Data
public class PersonDTO implements Serializable {
    private String name;

    private int age;

    List<AddressDTO> address;
}
