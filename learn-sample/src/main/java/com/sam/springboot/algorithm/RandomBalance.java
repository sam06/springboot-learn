package com.sam.springboot.algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @classname: RandomBalance
 * @description: TODO
 * @Author: sam
 * @date: 2022/5/22 8:09
 */
public class RandomBalance<T> implements Balance<T> {

    private Random random = new Random();
    @Override
    public T chooseOne(List<T> list) {
        int size = list.size();
        return list.get(random.nextInt(size));
    }

    public static void main(String[] args) {
        List<String> services = new ArrayList<>();

        services.add("192.168.0.1");
        services.add("192.168.0.2");
        services.add("192.168.0.3");
        services.add("192.168.0.4");
        services.add("192.168.0.5");

        RandomBalance<String> randomBalance = new RandomBalance();
        for (int i=0;i<services.size();i++){
            System.out.println(randomBalance.chooseOne(services));
        }
    }
}
