package com.sam.springboot.algorithm;

import java.util.List;

/**
 * @classname: Balance
 * @description: 轮询算法接口
 * @Author: sam
 * @date: 2022/5/22 8:08
 */
public interface Balance<T> {
    T chooseOne(List<T> list);
}
