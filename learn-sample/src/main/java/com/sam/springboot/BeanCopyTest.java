package com.sam.springboot;

import cn.hutool.core.bean.BeanUtil;
import com.sam.common.utils.BeanCopierUtil;
import com.sam.springboot.Bean.Address;
import com.sam.springboot.Bean.Person;
import com.sam.springboot.Bean.PersonDTO;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @classname: BeanCopyTest
 * @description: TODO
 * @Author: sam
 * @date: 2021/6/1 14:46
 */
public class BeanCopyTest {
    public static void main(String[] args) {
        Person person = new Person();
        person.setName("张三");
        person.setAge(20);

        List<Address> addressList = new ArrayList<>();
        Address address = new Address();
        address.setAddNo("30");
        address.setAddName("南京路");
        addressList.add(address);

        person.setAddress(addressList);

        System.out.println(person);
        System.out.println("===========BeanCopierUtil============");
        PersonDTO personDTO = BeanCopierUtil.copyProperties(person, PersonDTO.class);
        System.out.println(personDTO);
        System.out.println(personDTO.getAddress());

        System.out.println("=================BeanUtil====================");
        PersonDTO personDTO1 = BeanUtil.copyProperties(person, PersonDTO.class);
        System.out.println(personDTO1);
        System.out.println(personDTO1.getAddress());

        System.out.println("=================BeanUtil END====================");

        Duration d = Duration.ofMinutes(10000L);
        LocalTime hackUseOfClockAsDuration = LocalTime.MIN.plus(d);
        System.out.println(hackUseOfClockAsDuration);

        BigDecimal a = BigDecimal.ZERO;
        BigDecimal b = new BigDecimal("50");
        System.out.println(b.negate().add(a));
    }
}
