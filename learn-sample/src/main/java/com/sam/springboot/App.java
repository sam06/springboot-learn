package com.sam.springboot;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        /*System.out.println( "Hello World!" );
        BigDecimal bg=new BigDecimal("100");
        System.out.println(bg);
        bg.setScale(2,BigDecimal.ROUND_HALF_UP);
        System.out.println(bg);*/

        Scanner sc = new Scanner(System.in);
        System.out.print("请输入你的名字:");
        String name = sc.nextLine();  //读取字符串型输入
        System.out.print("请输入你的年龄:");
        int age = sc.nextInt();    //读取整型输入
        System.out.println("你好," + name +"\t" + "年龄:"+age );
    }
}
