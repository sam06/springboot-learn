package com.sam.springboot.reflect;

import com.sam.springboot.Bean.Address;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @classname: ObjectTest
 * @description: TODO
 * @Author: sam
 * @date: 2021/5/25 21:49
 */
public class ObjectTest {
    public static void main(String[] args) {
        List<Address> addressList = new ArrayList<>();
        Address address = new Address();
        address.setAddName("上海市");
        address.setAddNo("1001");
        addressList.add(address);

        Address address2 = new Address();
        address2.setAddName("北京市");
        address2.setAddNo("1002");
        addressList.add(address2);


    }

    /**
     * 是否是包装类型
     * @param typeName
     * @return
     */
    public static boolean isWarpType(String typeName){
        String[] types = {"java.lang.Integer",
                "java.lang.Double",
                "java.lang.Float",
                "java.lang.Long",
                "java.lang.Short",
                "java.lang.Byte",
                "java.lang.Boolean",
                "java.lang.Character",
                "java.lang.String"};
        return ArrayUtils.contains(types, typeName);
    }
}
