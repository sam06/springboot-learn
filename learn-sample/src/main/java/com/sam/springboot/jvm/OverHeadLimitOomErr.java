package com.sam.springboot.jvm;

import java.util.Map;
import java.util.Random;

/**
 * @classname: OverHeadLimitOomErr
 * @description: -Xms20m -Xmx20m
 * GC开销限制超过了错误，而GC多次未能清除它，这时便会引发java.lang.OutOfMemoryError。
 * java.lang.OutOfMemoryError: GC overhead limit exceeded
 *
 * 解决:
 * 要减少对象生命周期，尽量能快速的进行垃圾回收。
 * @Author: sam
 * @date: 2021/9/21 22:24
 */
public class OverHeadLimitOomErr {
    public static void main(String[] args) {
        Map map = System.getProperties();
        Random random = new Random();
        while (true){
            map.put(random.nextInt(),"test over oom");
        }
    }
}
