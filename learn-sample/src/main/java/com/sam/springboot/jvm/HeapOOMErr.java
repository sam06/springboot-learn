package com.sam.springboot.jvm;

import java.util.ArrayList;
import java.util.List;

/**
 * @classname: HeapOOMErr
 * @description: 演示jvm堆内存溢出
 * java.lang.OutOfMemoryError:Java heap space
 * 执行时jvm参数设置为-Xms20m -Xmx20m
 * @Author: sam
 * @date: 2021/9/21 21:30
 */
public class HeapOOMErr {
    public static void main(String[] args) {
        List<byte[]> list = new ArrayList<>();
        int i=0;
        while (true){
            list.add(new byte[5*1024*1024]);
            System.out.println("count is:"+(++i));
        }
    }
}
