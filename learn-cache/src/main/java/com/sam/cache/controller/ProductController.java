package com.sam.cache.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * @classname: ProductController
 * @description: TODO
 * @Author: sam
 * @date: 2021/8/22 10:25
 */
@RequestMapping("product")
@RestController
public class ProductController {

    @Autowired
    RedisTemplate redisTemplate;
    /**
     * 秒杀商品
     * @param pid
     * @return
     */
    @GetMapping("secKill/{pid}")
    public String secKill(@PathVariable String pid){

        Integer val = (Integer)redisTemplate.opsForValue().get(pid);
        if (val==0){
            return "库存不足！";
        }
        val-=1;
        redisTemplate.opsForValue().set(pid,val);

        return "ok";
    }

    @GetMapping("setStock/{pid}/{stockNum}")
    public void setStock(@PathVariable String pid,@PathVariable Integer stockNum){
        redisTemplate.opsForValue().set(pid,stockNum);
    }
}
