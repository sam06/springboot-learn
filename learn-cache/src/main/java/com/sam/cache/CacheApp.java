package com.sam.cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @classname: App
 * @description: TODO
 * @Author: sam
 * @date: 2021/8/22 10:24
 */
@SpringBootApplication
public class CacheApp {
    public static void main(String[] args) {
        SpringApplication.run(CacheApp.class,args);
    }
}
