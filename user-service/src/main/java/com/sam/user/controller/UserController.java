package com.sam.user.controller;

import com.sam.common.Result;
import com.sam.core.entity.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @classname: UserController
 * @description: TODO
 * @Author: sam
 * @date: 2021/3/23 10:51
 */
@RequestMapping("user")
@RestController
public class UserController {

    @GetMapping("list")
    public Result<List<User>> list()throws Exception{
        TimeUnit.SECONDS.sleep(2);
        return Result.ok(userList());
    }


    private List<User> userList(){
        List<User> userList = new ArrayList<>();
        for (int i=1;i<=1000;i++){
            User user = new User();
            user.setUserId("user"+1);
            user.setUsername("test name"+1);
            user.setPassword("11111111");
            user.setEmail(user.getUsername()+"@qq.com");
            user.setIdCard(UUID.randomUUID().toString());
            user.setAddress(System.currentTimeMillis()+"");
            userList.add(user);
        }
        return userList;
    }
}
